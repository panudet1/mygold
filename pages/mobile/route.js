import React from "react";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";

import Person from "@material-ui/icons/Person";
import AttachMoney from "@material-ui/icons/AttachMoney";
import MoneyOff from "@material-ui/icons/MoneyOff";
import MeetingRoom from "@material-ui/icons/MeetingRoom";

import Profile from "./profile";
import Withdraw from "./withdraw";
import Cancel from "./cancel";
import Main from "./main";
const PageMain = () => {
  const [value, setValue] = React.useState("หน้าหลัก");
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Router>
        <Switch>
          <Route path="/cancel">
            <Cancel />
          </Route>
          <Route path="/withdraw">
            <Withdraw />
          </Route>
          <Route path="/profile">
            <Profile />
          </Route>
          <Route path="/">
            <Main />
          </Route>
        </Switch>
        <BottomNavigation
          value={value}
          onChange={handleChange}
          className="container"
        >
          <BottomNavigationAction
            label="หน้าหลัก"
            value="หน้าหลัก"
            icon={
              <Link to="/">
                <MeetingRoom />
              </Link>
            }
          />
          <BottomNavigationAction
            label="ถอน"
            value="ถอน"
            icon={
              <Link to="/withdraw">
                <AttachMoney />
              </Link>
            }
          />
          <BottomNavigationAction
            label="ยกเลิก"
            value="ยกเลิก"
            icon={
              <Link to="/cancel">
                <MoneyOff />
              </Link>
            }
          />
          <BottomNavigationAction
            label="โปรไฟล์"
            value="โปรไฟล์"
            icon={
              <Link to="/profile">
                <Person />
              </Link>
            }
          />
        </BottomNavigation>
      </Router>
    </>
  );
};
export default PageMain;
