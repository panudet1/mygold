import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import axios, { post } from "axios";
const Page = () => {
  const router = useRouter();
  const [line_id, Line_id] = useState(String);

  try {
    useEffect(async () => {
      await liff.init({ liffId: `1655384823-M5awVAzb` }).catch((err) => {
        throw err;
      });
      if (liff.isLoggedIn()) {
        let getProfile = await liff.getProfile();
        const id = await getProfile.userId;
        await localStorage.setItem("auth", getProfile.userId);
        Line_id(await localStorage.getItem("auth"));
        await axios({
          method: "post",
          headers: {
            "Content-Type": "application/json",
          },
          url: "/api/index_mobile",
          data: JSON.stringify({ id }),
        }).then(
          (response) => {
            if (response.status === 200) {
              return router.push("/mobile/login");
            }
            if (response.status === 203) {
              return router.push("/mobile/confirm_password");
            }
            if (response.status === 201) {
              return router.push("/mobile/wait");
            }
            if (response.status === 202) {
              return router.push("/mobile/register");
            }
          },
          (error) => {
            console.log(error);
          }
        );
      } else {
        liff.login();
      }
    }, []);
  } catch (error) {}

  return <div>{line_id}</div>;
};
export default Page;
