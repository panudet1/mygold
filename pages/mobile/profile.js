import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import React, { useState, useEffect } from "react";
import axios, { post } from "axios";
import { useRouter } from "next/router";
import Modal from "react-bootstrap/Modal";
import moment from "moment";
import Swal from "sweetalert2";

export default function LabelBottomNavigation() {
  const [his, His] = useState([{}]);
  const [ranking, Ranking] = useState([{}]);
  const [show_ranking, Show_ranking] = useState(false);
  const [price, Price] = useState(String);
  const [installment, Installment] = useState(String);
  const [total_savings, Total_savings] = useState(String);
  const [weight, Weight] = useState(String);
  const [total_installment, Total_installment] = useState(String);
  useEffect(() => {
    LoadDataHis();
    RoomDetail();
  }, []);
  const RoomDetail = async () => {
    var data = {
      user_id: "id1",
      room_id: "fdssaveing_20201229_131000",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/joinroom/ranking",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        var Data = [];
        for (let index = 0; index < response.data.data.length; index++) {
          const element = response.data.data[index];
          const num = index + 1;
          var row = {
            num: "อันดับ : " + num,
            user_id: element.user_id,
            total_saving: element.total_savings,
          };
          Data.push(row);
        }
        Ranking(Data);
      },
      (error) => {
        console.log(error);
      }
    );
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_rooms/roomdata_by_id",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        // console.log(response.data.data[0].price);
        var number = Number(
          response.data.data[0].price.replace(/[^0-9.-]+/g, "")
        );
        Price(number);
        Installment(response.data.data[0].installment);
        Weight(response.data.data[0].weight);
      },
      (error) => {
        console.log(error);
      }
    );
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/joinroom/getdata_by_user_id",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        Total_savings(response.data.data[0].total_savings);
        Total_installment(response.data.data[0].total_installment);
      },
      (error) => {
        console.log(error);
      }
    );
  };
  const LoadDataHis = async () => {
    var data = {
      user_id: "id1",
      room_id: "fdssaveing_20201229_131000",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/bill/check_bill",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        var Data = [];
        for (let index = 0; index < response.data.data.length; index++) {
          const element = response.data.data[index];
          var sta = "";
          if (element.status == "pending") {
            sta = "กำลังตรวจสอบ";
          }
          if (element.status == "approved") {
            sta = "สำเร็จ";
          }
          if (element.status == "declined") {
            sta = "ไม่ผ่าน";
          }
          var row = {
            title: "อัพโหลดสลิป",
            when: moment(element.upload_date).format("YYYY-MM-DD HH:mm:ss"),
            status: sta,
          };
          Data.push(row);
        }
        His(Data);
      },
      (error) => {
        console.log(error);
      }
    );
  };
  return (
    <>
      <div
        style={{
          height: "90vh",
          backgroundColor: "rgb(156, 0, 0)",
          overflow: "scroll",
        }}
      >
        <div className="d-flex justify-content-center">
          <img
            src="/image/logo.svg"
            className="d-flex justify-content-center"
            width={"300px"}
            height={"150px"}
            //
          />
        </div>
        <div className="d-flex justify-content-center">
          <div>
            <p className="card-text" style={{ fontSize: "4vw", color: "#fff" }}>
              ข้อมูลโปรไฟล์
            </p>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-2">
          <div
            className="card"
            style={{
              width: "90vw",
              backgroundImage:
                "linear-gradient(rgb(255,255,255), rgb(213,213,213))",
            }}
          >
            <div className="card-body">
              <div className="container">
                <div className="row">
                  <div
                    className="col"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(37,37,37)",
                    }}
                  >
                    ทองสะสม
                  </div>
                  <div
                    className="col md-auto"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(231,180,0)",
                    }}
                  >
                    {parseFloat(
                      parseInt(price) / total_savings / 15.16
                    ).toFixed(2)}{" "}
                    กรัม
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-start mt-3 ">
          <div>
            <p
              className="card-text ml-3"
              style={{ fontSize: "4vw", color: "#fff" }}
            >
              ประวัติการชำระเงิน
            </p>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-2 mb-5">
          <div
            className="card p-2"
            style={{
              width: "90vw",
              minHeight: "10vh",
              backgroundImage:
                "linear-gradient(rgb(255,255,255), rgb(213,213,213))",
            }}
          >
            {his.map((item) => (
              <div className="station">
                {item.when} - {item.status}
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
