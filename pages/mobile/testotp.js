import React, { Component } from "react";

import Image from "next/image";
import axios, { post } from "axios";
import Swal from "sweetalert2";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Modal from "react-modal";
import InputAddress from "react-thailand-address-autocomplete";

import Router from "next/router";
const InputStyle = {
  backgroundColor: "rgb(236 ,216 ,146) ",
  color: "rgb(255 ,255 ,255) ",
  fontSize: "12pt",
  borderWidth: "0",
  fontFamily: "SukhumvitSet-SemiBold",
};
const InputStyleThailand = {
  height: "calc(1.5em + .75rem + 2px)",
  width: "100%",
  backgroundColor: "rgb(236 ,216 ,146) ",
  color: "rgb(255 ,255 ,255) ",
  fontSize: "12pt",
  borderWidth: "0",
  fontFamily: "SukhumvitSet-SemiBold",
};
const customStyles = {
  content: {},
};
const customStyles2 = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

const ButtonStyle = {
  backgroundColor: "rgb(153 ,0 ,0) ",
  color: "white",
  fontSize: "12pt",
};
const TitleStyle = {
  color: "rgb(0 ,0 ,0) ",
  fontSize: "18pt",
  fontFamily: "SukhumvitSet-SemiBold",
};
const TitleNameStyle = {
  color: "rgb(0 ,0 ,0) ",
  fontSize: "10pt",
  fontFamily: "SukhumvitSet-SemiBold",
};

class App extends Component {
  constructor(props) {
    super(props);
    this.textInputfname = React.createRef();
    this.textInputlname = React.createRef();
    this.textInputidcard = React.createRef();
    this.textInputbrithday = React.createRef();
    this.textInputexpiry = React.createRef();
    this.textInputbackidcard = React.createRef();
    this.textInputjob = React.createRef();
    this.textInputjob_location = React.createRef();
    this.textInputaddress = React.createRef();
    this.textInputsubdistrict = React.createRef();
    this.textInputdistrict = React.createRef();
    this.textInputprovince = React.createRef();
    this.textInputzipcode = React.createRef();

    this.textInputphonenumber = React.createRef();
    this.state = {
      logo_path: "/image/logo.svg",
      fname: "",
      lname: "",
      phonenumber: "",
      idcard: "",
      brithday: null,
      expiry: null,
      backidcard: "",
      job: "",
      job_location: "",
      address: "",
      subdistrict: "",
      district: "",
      province: "",
      zipcode: "",

      user_line_id: "",
      is_checked: false,
      fileIdCard: "",
      fileUserWithIdCard: "",
      imageIdCard: "",
      imageUserWithIdCard: "",
      modal_show: false,
      condition_show: false,
      otpconfirm_show: false,
      page_1: false,
      page_2: true,
      refcode: "",
      otpconfirm: "",
      regexp: /^[0-9\b]+$/,
    };
  }

  componentDidMount = async () => {
    // this.setState({
    //   user_line_id: await localStorage.getItem("auth"),
    // });
  };
  imageIdCardChange(e) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        fileIdCard: file,
        imageIdCard: reader.result,
      });
    };
    reader.readAsDataURL(file);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  onSelect(fullAddress) {
    const { subdistrict, district, province, zipcode } = fullAddress;
    this.setState({
      subdistrict,
      district,
      province,
      zipcode,
    });
  }

  imageUserWithIdCardChange(e) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        fileUserWithIdCard: file,
        imageUserWithIdCard: reader.result,
      });
    };
    reader.readAsDataURL(file);
  }
  onChecked = () => {
    var that = this.state;
    if (that.is_checked === true) {
      this.setState({ is_checked: false });
    } else {
      this.setState({ is_checked: true });
    }
  };
  onRegister = async () => {
    var that = this.state;
    var fileIdCardPath = await this.fileUpload(that.fileIdCard).then(
      (response) => {
        return response.data.secure_url;
      }
    );
    var fileUserWithIdCardPath = await this.fileUpload(
      that.fileUserWithIdCard
    ).then((response) => {
      return response.data.secure_url;
    });
    var data = {
      fname: that.fname,
      lname: that.lname,
      phonenumber: that.phonenumber,
      idcard: that.idcard,
      brithday: that.brithday,
      expiry: that.expiry,
      backidcard: that.backidcard,
      job: that.job,
      job_location: that.job_location,
      address: that.address,
      province: that.province,
      district: that.district,
      canton: that.subdistrict,
      postal_code: that.zipcode,
      user_line_id: "Uf08604dbfae9ffdc8123032a8cb2d26c",
      fileIdCard: fileIdCardPath,
      fileUserWithIdCard: fileUserWithIdCardPath,
      PIN: "",
      status: "pending",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/register",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        this.setState({ modal_show: false });
        Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "กรุณารอการยืนยันในภายหลัง",
        });
        return Router.push("/mobile/wait");
      },
      (error) => {
        console.log(error);
      }
    );
  };
  fileUpload = (file) => {
    const url = "https://api.cloudinary.com/v1_1/zxv/image/upload";
    const formData = new FormData();
    formData.append("file", file);
    formData.append("upload_preset", "register");
    return axios.post(url, formData);
  };

  onCheckOTP = async () => {
    console.log("check OTP");
    this.setState({ otpconfirm_show: false });
    // otpconfirm_show
    axios
      .get("/api/OTP", {
        params: {
          otp: this.state.otpconfirm,
          ref: this.state.refcode,
        },
      })
      .then(
        (response) => {
          console.log(response.status);
          if (response.status === 200) {
            this.setState({ modal_show: true });
            this.onRegister();
          }
          if (response.status === 201) {
            this.setState({
              otpconfirm: "",
            });
            return Swal.fire({
              icon: "error",
              title: "ผิดพลาด",
              text: "รหัส OTP ไม่ถูกต้อง!",
            });
          }
        },
        (error) => {
          console.log(error);
        }
      );
  };

  onSendOTP = async () => {
    var OTPnumber = "";
    var possible = "0123456789";
    for (var i = 0; i < 6; i++) {
      OTPnumber += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    var Ref = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 6; i++) {
      Ref += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    var messages = "รหัสอ้างอิง : " + Ref + "\r\n" + "รหัส OTP : " + OTPnumber;
    var data = {
      otp: OTPnumber,
      ref: Ref,
    };
    console.log(data);
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/OTP",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
    this.setState({
      refcode: Ref,
      otpconfirm_show: true,
    });
    var phone = await this.state.phonenumber;
    axios
      .get("http://www.thsms.com/api/rest", {
        params: {
          method: "send",
          username: "tectony",
          password: "&1Va64vBHq5F99AJ",
          from: "OTP",
          to: phone,
          message: messages,
        },
      })
      .then(function (response) {})
      .catch(function (error) {
        console.log(error);
      });
  };

  onNextPage = async () => {
    var that = this.state;
    if (that.fname === "") {
      this.textInputfname.current.focus();
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ ชือ!",
      });
    }
    if (that.lname === "") {
      this.textInputlname.current.focus();
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ นามสกุล!",
      });
    }
    if (that.idcard === "") {
      this.textInputidcard.current.focus();
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ เลขบัตรประชาชน!",
      });
    }
    if (that.brithday === null) {
      this.textInputbrithday.input.focus();
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ วันเกิด!",
      });
    }
    if (that.expiry === null) {
      this.textInputexpiry.input.focus();
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ วันหมดอายุบัตรประชาชน!",
      });
    }
    if (that.backidcard === "") {
      this.textInputbackidcard.current.focus();
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ เลขหลังบัตรประชาชน!",
      });
    }
    if (that.job === "") {
      this.textInputjob.current.focus();
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ อาชีพ!",
      });
    }
    if (that.job_location === "") {
      this.textInputjob_location.current.focus();
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ ที่ทำงาน!",
      });
    }
    if (that.address === "") {
      this.textInputaddress.current.focus();
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ ทีอยู่!",
      });
    }
    if (that.subdistrict === "") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ ตำบล/แขวง!",
      });
    }
    if (that.district === "") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ อำเภอ/เขต!",
      });
    }
    if (that.province === "") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ จังหวัด!",
      });
    }
    if (that.zipcode === "") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ รหัสไปรษณีย์!",
      });
    }
    if (that.fileIdCard === "") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ อัพโหลดรูปบัตรประชาชน!",
      });
    }
    if (that.imageUserWithIdCard === "") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุ อัพโหลดรูปถ่ายคู่บัตรประชาชน!",
      });
    }
    if (that.is_checked === false) {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณายอมรับเงื่อนไข!",
      });
    }

    this.setState({
      page_1: true,
      page_2: false,
    });
  };
  onBackPage = async () => {
    this.setState({
      page_1: false,
      page_2: true,
    });
  };

  onChangeIdCard = (e) => {
    const { value, maxLength } = e.target;
    const message = value.slice(0, 13);
    if (isNaN(message)) {
    } else {
      this.setState({
        idcard: message,
      });
    }
  };

  render() {
    let { imageIdCard } = this.state;
    let { imageUserWithIdCard } = this.state;
    let $imagePreviewIdCard = null;
    if (imageIdCard) {
      $imagePreviewIdCard = (
        <img
          src={imageIdCard}
          width="300px"
          height="200px"
          alt=""
          style={{ marginLeft: "10px" }}
        />
      );
    } else {
      $imagePreviewIdCard = (
        <img
          src="/image/register/บัตรประชาชน.jpg"
          width="300px"
          height="200px"
          alt=""
          style={{ marginLeft: "10px" }}
        />
      );
    }
    let $imagePreviewUserWithIdCard = null;
    if (imageUserWithIdCard) {
      $imagePreviewUserWithIdCard = (
        <img
          src={imageUserWithIdCard}
          width="300px"
          height="200px"
          alt=""
          style={{ marginLeft: "10px" }}
        />
      );
    } else {
      $imagePreviewUserWithIdCard = (
        <img
          src="/image/register/ภาพถ่ายคู่บัตร.png"
          width="300px"
          height="200px"
          alt=""
          style={{ marginLeft: "10px" }}
        />
      );
    }

    return (
      <>
        {/* /////////////////////// */}
        <div className="tab-content mb-5" hidden={this.state.page_1}>
          <div className="container mt-5">
            <div className="d-flex justify-content-center mb-3">
              <Image
                src={this.state.logo_path}
                alt="Picture of the author"
                width={300}
                height={100}
              />
            </div>
            <div className="mb-3">
              <label
                htmlFor="exampleInputEmail1"
                className="form-label"
                style={TitleStyle}
              >
                สร้างบัญชี
              </label>
            </div>

            <div className="mb-3">
              <label
                htmlFor="exampleInputEmail1"
                className="form-label"
                style={TitleNameStyle}
              >
                ข้อมูลส่วนตัว
              </label>
            </div>
            <div className="mb-3">
              <input
                ref={this.textInputfname}
                type="text"
                className="form-control InputRegister"
                placeholder="ชื่อจริง"
                onChange={(e) => this.setState({ fname: e.target.value })}
                style={InputStyle}
              />
            </div>
            <div className="mb-3">
              <input
                ref={this.textInputlname}
                type="text"
                className="form-control InputRegister"
                placeholder="นามสกุล"
                onChange={(e) => this.setState({ lname: e.target.value })}
                style={InputStyle}
              />
            </div>
            <div className="mb-3">
              <label
                htmlFor="exampleInputEmail1"
                className="form-label"
                style={TitleNameStyle}
              >
                ข้อมูลบัตรประชาชน
              </label>
            </div>
            <div className="mb-3">
              <input
                ref={this.textInputidcard}
                type="tel"
                className="form-control InputRegister"
                placeholder="หมายเลขบัตรประชาชน"
                value={this.state.idcard}
                onChange={this.onChangeIdCard}
                // onChange={}
                style={InputStyle}
              />
            </div>

            <div className="mb-3">
              <div className="customDatePickerWidth">
                <DatePicker
                  ref={(datepicker) => {
                    this.textInputbrithday = datepicker;
                  }}
                  // Ref={this.textInputbrithday}
                  className="form-control InputRegister"
                  placeholderText="วัน/เดือน/ปี เกิด"
                  onChange={(date) => this.setState({ brithday: date })}
                  selected={this.state.brithday}
                />
              </div>
            </div>
            <div className="mb-3">
              <div className="customDatePickerWidth">
                <DatePicker
                  ref={(datepicker) => {
                    this.textInputexpiry = datepicker;
                  }}
                  className="form-control InputRegister"
                  placeholderText="วันหมดอายุบัตรประชาชน"
                  onChange={(date) => this.setState({ expiry: date })}
                  selected={this.state.expiry}
                />
              </div>
            </div>
            <div className="mb-3">
              <input
                ref={this.textInputbackidcard}
                type="text"
                className="form-control InputRegister"
                maxLength="14"
                placeholder="เลขหลังบัตรประจำตัวประชาชน"
                onChange={(e) => this.setState({ backidcard: e.target.value })}
                style={InputStyle}
              />
            </div>

            <div className="mb-3">
              <label
                htmlFor="exampleInputEmail1"
                className="form-label"
                style={TitleNameStyle}
              >
                ข้อมูลอาชีพ
              </label>
            </div>
            <div className="mb-3">
              <input
                ref={this.textInputjob}
                type="text"
                className="form-control InputRegister"
                placeholder="ระบุอาชีพ"
                onChange={(e) => this.setState({ job: e.target.value })}
                style={InputStyle}
              />
            </div>
            <div className="mb-3">
              <input
                ref={this.textInputjob_location}
                type="text"
                className="form-control InputRegister"
                placeholder="สถานที่ทำงาน"
                onChange={(e) =>
                  this.setState({ job_location: e.target.value })
                }
                style={InputStyle}
              />
            </div>

            <div className="mb-3">
              <label
                htmlFor="exampleInputEmail1"
                className="form-label"
                style={TitleNameStyle}
              >
                ที่อยู่ปัจจุบัน
              </label>
            </div>
            <div className="mb-3">
              <input
                ref={this.textInputaddress}
                type="text"
                className="form-control InputRegister"
                placeholder="บ้านเลขที่และชื่อถนน"
                onChange={(e) => this.setState({ address: e.target.value })}
                style={InputStyle}
              />
            </div>
            <div className="custom-thailand  mb-3">
              <InputAddress
                address="subdistrict"
                className="InputRegister"
                placeholder="แขวง/ตำบล"
                value={this.state.subdistrict}
                onChange={(e) => this.onChange(e)}
                onSelect={(e) => this.onSelect(e)}
                style={InputStyleThailand}
              />
            </div>
            <div className="custom-thailand mb-3">
              <InputAddress
                className="InputRegister"
                address="district"
                placeholder="เขต / อำเภอ"
                value={this.state.district}
                onChange={(e) => this.onChange(e)}
                onSelect={(e) => this.onSelect(e)}
                style={InputStyleThailand}
              />
            </div>
            <div className="custom-thailand mb-3">
              <InputAddress
                className="InputRegister"
                address="province"
                placeholder="จังหวัด"
                value={this.state.province}
                onChange={(e) => this.onChange(e)}
                onSelect={(e) => this.onSelect(e)}
                style={InputStyleThailand}
              />
            </div>
            <div className="custom-thailand mb-3">
              <InputAddress
                className="InputRegister"
                address="zipcode"
                placeholder="รหัสไปรษณีย์"
                value={this.state.zipcode}
                onChange={(e) => this.onChange(e)}
                onSelect={(e) => this.onSelect(e)}
                style={InputStyleThailand}
              />
            </div>

            <div className="mb-3">
              <label
                htmlFor="exampleInputEmail1"
                className="form-label"
                style={TitleNameStyle}
              >
                *รูปบัตรประชาชน
              </label>
            </div>
            <div className="d-flex justify-content-center mb-3">
              {$imagePreviewIdCard}
            </div>
            <input
              className="fileInput"
              type="file"
              accept="image/x-png,image/gif,image/jpeg"
              onChange={(e) => this.imageIdCardChange(e)}
            />
            <div className="mb-3">
              <label
                htmlFor="exampleInputEmail1"
                className="form-label"
                style={TitleNameStyle}
              >
                *รูปถ่ายคู่บัตรประชาชน
              </label>
            </div>
            <div className="d-flex justify-content-center mb-3">
              {$imagePreviewUserWithIdCard}
            </div>
            <input
              className="fileInput"
              type="file"
              accept="image/x-png,image/gif,image/jpeg"
              onChange={(e) => this.imageUserWithIdCardChange(e)}
            />
            <div className="mb-3 form-check mt-4">
              <input
                type="checkbox"
                className="form-check-input"
                id="exampleCheck1"
                checked={this.is_checked}
                onChange={this.onChecked}
              />
              <label
                className="form-check-label "
                htmlFor="exampleCheck1"
                style={TitleNameStyle}
              >
                ข้าพเจ้ายอมรับ เงื่อนไขและข้อตกลง ของบริษัท แม่อุ๋ยออมทอง จำกัด
              </label>
            </div>
            <button
              className="btn btn-danger container mb-3"
              // onClick={this.onNextPage}
              onClick={() => this.setState({ condition_show: true })}
              style={ButtonStyle}
            >
              อ่านเงื่อนไข
            </button>
            <button
              className="btn btn-danger container"
              onClick={this.onNextPage}
              style={ButtonStyle}
            >
              ต่อไป
            </button>
          </div>
        </div>
        <div className="tab-content mb-5" hidden={this.state.page_2}>
          <div className="container mt-5">
            <div className="d-flex justify-content-center mb-3">
              <Image
                src={this.state.logo_path}
                alt="Picture of the author"
                width={300}
                height={100}
              />
            </div>
            <div className="mb-3">
              <label
                htmlFor="exampleInputEmail1"
                className="form-label"
                style={TitleStyle}
              >
                สร้างบัญชี
              </label>
            </div>

            <div className="mb-3">
              <label
                htmlFor="exampleInputEmail1"
                className="form-label"
                style={TitleNameStyle}
              >
                ยืนยันตัวตน
              </label>
            </div>
            <div className="mb-3">
              <input
                type="text"
                className="form-control InputRegister"
                placeholder="เบอร์โทรศัพท์มือถือ"
                onChange={(e) => this.setState({ phonenumber: e.target.value })}
                style={InputStyle}
              />
            </div>
            <button
              className="btn btn-danger container mb-3"
              onClick={this.onSendOTP}
              style={ButtonStyle}
            >
              ส่งรหัส OTP
            </button>
            <button
              className="btn btn-danger container mb-3"
              onClick={this.onBackPage}
              style={ButtonStyle}
            >
              ย้อนกลับ
            </button>
          </div>
        </div>
        <Modal style={customStyles2} isOpen={this.state.modal_show}>
          <span
            className="spinner-grow spinner-grow-sm"
            role="status"
            aria-hidden="true"
          />
          กรุณารอสักครู่...
        </Modal>

        <Modal
          isOpen={this.state.condition_show}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <div className="container" style={{ maxHeight: "300px" }}>
            {/* Modal Header */}
            <div className="modal-header">
              <h1 className="modal-title" style={TitleStyle}>
                เงื่อนไขและข้อตกลง
              </h1>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                onClick={() => this.setState({ condition_show: false })}
              >
                ×
              </button>
            </div>
            <div className="modal-body">
              <p style={TitleNameStyle}>
                การซื้อทองคำ เงินออมงวดแรกชำระเป็นเงินสด
                งวดต่อไปชำระทุกวันทำการที่ 1 เวลา 14.00 น. ของทุกเดือน
                โดยบริษัทจะหักเงินฝากผ่านบัญชีเงินฝากอัตโนมัติ
                บริษัทจะซื้อทองคำทุกวันทำการที่ 2
                ของทุกเดือนโดยอ้างอิงราคาทองคำขายออกของฮั่วเซ่งเฮง ณ เวลา 17.30
                น. บริษัท คิดหน่วยการซื้อทองคำเป็นกรัมละบาท (ทศนิยม 4 หลัก
                ตัดตำแหน่ง ที่ 5 ทิ้ง) โดยทองคำ 1 บาท เท่ากับ 15.244 กรัม
                บริษัทจะออกใบเสร็จรับเงินหลังจากที่ได้ทำการหักบัญชีเงินฝากอัตโนมัติ
                (ATS)ของลูกค้าเรียบร้อยแล้ว
                และบริษัทจะออกใบยืนยันการซื้อขายทองคำให้แก่ลูกค้าทุกครั้งที่มีการซื้อขายภายในเวลา
                7 วัน ผ่านทางไปรษณีย์หรือ E – mail การรับทองคำ หรือ ขายคืนทองคำ
                เมื่อสะสมหน่วยทองคำครบ ลูกค้าสามารถขอรับทองคำได้
                โดยทองคำขั้นต่ำที่สามารถรับได้ 1 สลึง
                หรือสามารถขายคืนหน่วยทองคำแบบเต็มจำนวนและรับคืนเป็นเงิน
                โดยบริษัทจะโอนเงินเข้าบัญชีของลูกค้าตามที่ลูกค้าได้แจ้งไว้
                ราคาขายคืนทองคำหรือหน่วยทองคำอ้างอิงราคาซื้อคืนของฮั่วเซ่งเฮง ณ
                เวลาที่สั่งขายของวันที่ลูกค้าแจ้งขาย และรับเงินค่าขายภายใน 2
                วันทำการ (T + 2) การปรับเพิ่มหรือลดขนาดการออม
                ลูกค้าสามารถปรับเพิ่มหรือลดขนาดของเงินออมขั้นต่ำ 1,000 บาท
                โดยแจ้งให้บริษัททราบล่วงหน้าอย่างน้อย 2 วันทำการ
                ก่อนวันหักผ่านบัญชีอัตโนมัติ (ATS)
                การขอเปลี่ยนแปลงบัญชีที่ใช้หักบัญชีเงินฝากอัตโนมัติ (ATS)
                ลูกค้าสามารถขอเปลี่ยนแปลงบัญชีที่ใช้หักบัญชีเงินฝากอัตโนมัติ
                (ATS) โดยแจ้งล่วงหน้าเป็นลายลักษณ์อักษรอย่างน้อย 7 วันทำการ
                ก่อนวันหักบัญชีเงินฝากอัตโนมัติ (ATS) ในเดือนถัดไป
                การยกเลิกบัญชีการออม โดยแจ้งล่วงหน้าอย่างน้อย 7
                วันทำการก่อนวันหักบัญชีเงินฝากอัตโนมัติ (ATS) ในเดือนถัดไป
                และขอรับทองคำตามจำนวนที่ได้ทำการออมไว้
                โดยเศษทองคำจะคำนวณและคืนเป็นเงินตามข้อกำหนดของบริษัท
              </p>
            </div>

            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-danger"
                data-dismiss="modal"
                onClick={() => this.setState({ condition_show: false })}
              >
                ปิด
              </button>
            </div>
          </div>
        </Modal>

        <Modal
          isOpen={this.state.otpconfirm_show}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <div className="container" style={{ maxHeight: "300px" }}>
            <div className="modal-header">
              <h1 className="modal-title" style={TitleStyle}>
                กรอกรหัส OTP
              </h1>
            </div>
            <div className="modal-body">
              <p style={TitleNameStyle}>รหัสอ้างอิง : {this.state.refcode}</p>
              <input
                type="text"
                className="form-control InputRegister"
                placeholder="หมายเลข OTP 6 หลัก"
                onChange={(e) => this.setState({ otpconfirm: e.target.value })}
                style={InputStyle}
              />
            </div>

            <div className="modal-footer">
              <button
                className="btn btn-danger container mb-3 mt-3"
                onClick={this.onSendOTP}
                style={ButtonStyle}
              >
                ส่งรหัส OTP อีกครั้ง
              </button>
              <button
                className="btn btn-danger container"
                onClick={() => this.setState({ otpconfirm_show: false })}
                style={ButtonStyle}
              >
                ยกเลิก
              </button>
              <button
                className="btn btn-danger container"
                onClick={this.onCheckOTP}
                style={ButtonStyle}
              >
                ยืนยัน
              </button>
            </div>
          </div>
        </Modal>
        {/* /////////////////////// */}
      </>
    );
  }
}

export default App;
