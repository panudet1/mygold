import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import React, { useState, useEffect } from "react";
import axios, { post } from "axios";
import { useRouter } from "next/router";
import Modal from "react-bootstrap/Modal";
import moment from "moment";
import Swal from "sweetalert2";
import TimePicker from "react-bootstrap-time-picker";

export default function LabelBottomNavigation() {
  //   console.log();TextField
  const router = useRouter();
  const [room_id, Room_id] = useState(localStorage.getItem("Room_id"));
  const [money, Money] = useState(String);
  const [image_slip, Image_slip] = useState(String);
  const [time, Time] = useState(String);
  const [hidealert, Hidealert] = useState(true);
  const [image_slip_render, Image_slip_render] = useState(null);
  const [file_bill, File_bill] = useState("");
  const [imagePreviewIdCard, ImagePreviewIdCard] = useState(
    <img
      src="/image/no-image.jpg"
      width="300px"
      height="200px"
      alt=""
      style={{ marginLeft: "10px" }}
    />
  );
  const GoBack = async () => {
    return router.push("/mobile/saving_room");
  };

  const onChangeMoney = (e) => {
    const { value } = e.target;
    const message = value.slice(0, 13);
    if (isNaN(message)) {
    } else {
      Money(message);
    }
  };
  const onConfrimeSlip = async () => {
    if (file_bill === "") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "คุณยังไม่อัพโหลดสลิป",
      });
    }
    if (money === "") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุจำนวนเงิน",
      });
    }
    if ((await parseInt(money)) < 100) {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "จำนวนเงินขั้นต่ำ 100 บาท",
      });
    }
    if (time === "") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุเวลาทำรายการโอน",
      });
    }
    var Bill_Path = await fileUpload(file_bill).then((response) => {
      return response.data.secure_url;
    });
    var data = {
      user_id: "id1",
      bill_path: Bill_Path,
      money: money,
      time: time,
      room_id: "fdssaveing_20201229_131000",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/bill/upload",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "กรุณารอการยืนยันในภายหลัง",
        });
        return router.push("/mobile/saving_room");
      },
      (error) => {
        console.log(error);
      }
    );
  };

  const fileUpload = (file) => {
    // const url = "/api/upload_bill";
    // const formData = new FormData();
    // formData.append("file", file, file.name);
    // formData.append("upload_presets", "upload");

    // const config = {
    //   headers: {
    //     "Content-Type": "multipart/form-data",
    //   },
    // };
    // return axios.post(url, formData, config);

    const url = "https://api.cloudinary.com/v1_1/zxv/image/upload";
    const formData = new FormData();
    formData.append("file", file);
    formData.append("upload_preset", "upload_bill");
    return axios.post(url, formData);
  };
  const onCheckMin = async () => {
    if ((await parseInt(money)) < 100) {
      await Hidealert(false);
    } else {
      await Hidealert(true);
    }
  };
  const imageIdCardChange = async (e) => {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      File_bill(file);
      ImagePreviewIdCard(
        <img
          src={reader.result}
          width="300px"
          height="200px"
          alt=""
          style={{ marginLeft: "10px" }}
        />
      );
    };
    reader.readAsDataURL(file);
  };
  return (
    <>
      <div
        style={{
          height: "100vh",
          backgroundColor: "rgb(156, 0, 0)",
          overflow: "scroll",
        }}
      >
        <div className="d-flex justify-content-center">
          <img
            src="/image/logo.svg"
            className="d-flex justify-content-center"
            width={"300px"}
            height={"150px"}
            //
          />
        </div>
        <div className="d-flex justify-content-center pl-3 pr-3">
          <IconButton
            style={{ backgroundColor: "#fff", padding: "0" }}
            className="mr-auto"
            onClick={GoBack}
          >
            <ChevronLeftIcon
              style={{ fontSize: 40, color: "rgb(156, 0, 0)" }}
            />
          </IconButton>
        </div>
        <div className="d-flex justify-content-center mt-2">
          <div className="d-flex justify-content-center mb-3">
            {imagePreviewIdCard}
          </div>
        </div>
        <div className="d-flex justify-content-center mt-2">
          <input
            className="fileInputcustom"
            type="file"
            id="file"
            accept="image/x-png,image/gif,image/jpeg"
            onChange={(e) => imageIdCardChange(e)}
          />
          {/* <button htmlFor="file" type="button" className="btn btn-light">
            Light
          </button> */}

          <label htmlFor="file" type="button" className="btn btn-light">
            เลือกรูปภาพ
          </label>
        </div>

        <div className="d-flex justify-content-center mt-2">
          <div className="modal-body ">
            {/* <div className="container">
              <div className="row">
                <div className="col">1 of 3</div>
                <div className="col-md-auto">บาท</div>
              </div>
            </div> */}
            <div className="d-flex bd-highlight">
              <div className="p-2 flex-grow-1 bd-highlight">
                <input
                  type="text"
                  className=" inputUpload d-flex justify-content-center"
                  style={{ color: "#fff", width: "80vw" }}
                  placeholder={"ขั้นต่ำ 100 บาท"}
                  value={money}
                  onChange={(e) => onChangeMoney(e)}
                  onMouseLeave={() => onCheckMin()}
                />
              </div>
              <div
                className="p-2 bd-highlight"
                style={{ color: "#fff", width: "10vw" }}
              >
                บาท
              </div>
            </div>
            <p
              style={{
                fontFamily: "SukhumvitSet-SemiBold",
                color: "#F1C40F",
                margin: "0px",
                padding: "0px",
              }}
              hidden={hidealert}
            >
              *ขั้นต่ำ 100 บาท
            </p>
            <div className="d-flex bd-highlight">
              <div className="p-2 flex-grow-1 bd-highlight">
                <input
                  type="text"
                  className=" inputUpload d-flex justify-content-center mt-2"
                  style={{ color: "#fff", width: "90vw" }}
                  placeholder={"ระบุเวลาตามสลิปโอนเงิน"}
                  value={time}
                  onChange={(e) => Time(e.target.value)}
                  // onMouseLeave={() => onCheckMin()}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-2 mb-5">
          <button
            type="button"
            className="btn d-flex justify-content-center align-items-center"
            style={{
              backgroundColor: "rgb(56, 0, 0)",
              width: "90vw",
              height: "10vh",
              fontFamily: "SukhumvitSet-SemiBold",
              color: "#fff",
              textAlign: "center",
            }}
            onClick={onConfrimeSlip}
          >
            ยืนยันการชำระ
          </button>
        </div>
      </div>
    </>
  );
}
