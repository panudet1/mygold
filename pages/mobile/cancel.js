import React, { useState, useEffect } from "react";
import axios, { post } from "axios";
import { useRouter } from "next/router";
import Modal from "react-bootstrap/Modal";
import moment from "moment";
import Swal from "sweetalert2";
const ButtonStyle = {
  borderRadius: "10vw",
  width: "20vw",
  height: "20vw",
  backgroundColor: "#fff",
  border: "2px solid #D5D8DC",
  fontSize: "10vw",
  color: "#7e7e7e",
  textAlign: "center",
};
export default function LabelBottomNavigation() {
  const [price, Price] = useState(String);

  const [fee, Fee] = useState(String);
  const [total_savings, Total_savings] = useState(String);
  const [btn_cancel, Btn_cancel] = useState(true);
  const [gold, Gold] = useState(false);

  const [dot_1, Dot_1] = useState("dot ml-1 mr-1");
  const [dot_2, Dot_2] = useState("dot ml-1 mr-1");
  const [dot_3, Dot_3] = useState("dot ml-1 mr-1");
  const [dot_4, Dot_4] = useState("dot ml-1 mr-1");
  const [dot_5, Dot_5] = useState("dot ml-1 mr-1");
  const [dot_6, Dot_6] = useState("dot ml-1 mr-1");
  const [PIN, SetPin] = useState("");
  const [confrim, Confrim] = useState(true);
  const [unready_text, Unready_text] = useState(String);
  const [pending, Pending] = useState(true);
  useEffect(() => {
    load_savingroom();
    check_finished();
  }, []);

  const check_finished = async () => {
    var data = {
      room_id: "fdssaveing_20201229_131000",
      user_id: "id1",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/joinroom/check_finished",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log("สถานะออม : " + response.status);
        if (response.status == 200) {
          //ออมครบ
          Pending(true);
          Unready_text(
            "คุณไม่สามารถทำการยกเลิกได้ เนื่องจากคุณออมครบตามจำนวนที่กำหนดแล้ว"
          );
        } else {
          //ออมไม่ครบ
          Pending(false);
          check_pending();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  };
  const check_pending = async () => {
    var data = {
      user_id: "id1",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/cancel/check_pending",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response);
        if (response.status == 200) {
          Pending(true);
          Unready_text("กำลังดำเนินการ รอการตอบกลับในภายหลัง");
        } else {
          Pending(false);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  };
  const load_savingroom = async () => {
    var data = {
      room_id: "fdssaveing_20201229_131000",
      user_id: "id1",
    };

    var fee_full = await axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_rooms/roomdata_by_id",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        var weight = response.data.data[0].weight;
        // Weight(response.data.data[0].weight);
        var full_bath = 0;
        if (weight == "ครึ่งสลึง") {
          full_bath = 1 / 8;
        }
        if (weight == "1 สลึง") {
          full_bath = 1 / 4;
        }
        if (weight == "2 สลึง") {
          full_bath = 1 / 2;
        }
        if (weight == "1 บาท") {
          full_bath = 1;
        }

        var number = Number(
          response.data.data[0].price.replace(/[^0-9.-]+/g, "")
        );
        Price(number * full_bath);
        Fee(response.data.data[0].fee);
        return response.data.data[0].fee;
      },
      (error) => {
        console.log(error);
      }
    );
    var total = await axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/joinroom/getdata_by_user_id",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        Total_savings(response.data.data[0].total_savings);
        return response.data.data[0].total_savings;
      },
      (error) => {
        console.log(error);
      }
    );

    console.log(parseInt(total), parseInt(fee_full));
    if (parseInt(total) <= parseInt(fee_full)) {
      Btn_cancel(true);
    } else {
      Btn_cancel(false);
    }
  };

  const onClickBath = async () => {
    Gold(false);
  };
  const onClickGold = async () => {
    Gold(true);
  };

  const onRequestCancel = async () => {
    var type = "";
    var total = "";
    if (gold) {
      type = "gold";
      total = parseFloat(price / total_savings / 15.16).toFixed(2) + " กรัม";
    } else {
      type = "money";
      total = parseInt(total_savings) - parseInt(fee);
    }

    var data = {
      room_id: "fdssaveing_20201229_131000",
      user_id: "id1",
      type: type,
      total: total,
    };
    console.log(data);
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/cancel/request_cancel",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response);
        check_pending();
      },
      (error) => {
        console.log(error);
      }
    );
  };
  const onConfirmPass = async () => {
    Confrim(false);
  };
  const closeConfrim = async () => {
    Confrim(true);
  };
  const OnPinPress = async (number) => {
    var Ole_pin = await PIN;
    if (number == "x") {
      if (Ole_pin.length == 1) {
        var str = Ole_pin;
        str = str.substring(0, str.length - 1);
        Dot_1("dot ml-1 mr-1");
        SetPin(str);
      }
      if (Ole_pin.length == 2) {
        var str = Ole_pin;
        str = str.substring(0, str.length - 1);
        Dot_2("dot ml-1 mr-1");
        SetPin(str);
      }
      if (Ole_pin.length == 3) {
        var str = Ole_pin;
        str = str.substring(0, str.length - 1);
        Dot_3("dot ml-1 mr-1");
        SetPin(str);
      }
      if (Ole_pin.length == 4) {
        var str = Ole_pin;
        str = str.substring(0, str.length - 1);
        Dot_4("dot ml-1 mr-1");
        SetPin(str);
      }
      if (Ole_pin.length == 5) {
        var str = Ole_pin;
        str = str.substring(0, str.length - 1);
        Dot_5("dot ml-1 mr-1");
        SetPin(str);
      }
      if (Ole_pin.length == 6) {
        var str = Ole_pin;
        str = str.substring(0, str.length - 1);
        Dot_6("dot ml-1 mr-1");
        SetPin(str);
      }
    } else {
      if (Ole_pin.length == 0) {
        Dot_1("dot_full ml-1 mr-1");
        SetPin((await Ole_pin) + number);
      }
      if (Ole_pin.length == 1) {
        Dot_2("dot_full ml-1 mr-1");
        SetPin((await Ole_pin) + number);
      }
      if (Ole_pin.length == 2) {
        Dot_3("dot_full ml-1 mr-1");
        SetPin((await Ole_pin) + number);
      }
      if (Ole_pin.length == 3) {
        Dot_4("dot_full ml-1 mr-1");
        SetPin((await Ole_pin) + number);
      }
      if (Ole_pin.length == 4) {
        Dot_5("dot_full ml-1 mr-1");
        SetPin((await Ole_pin) + number);
      }
      if (Ole_pin.length == 5) {
        Dot_6("dot_full ml-1 mr-1");
        SetPin((await Ole_pin) + number);
        var data = {
          user_id: "Uf08604dbfae9ffdc8123032a8cb2d26c",
          password: (await Ole_pin) + number,
        };
        await axios({
          method: "post",
          headers: {
            "Content-Type": "application/json",
          },
          url: "/api/login_mobile",
          data: JSON.stringify({ data }),
        }).then(
          (response) => {
            if (response.status === 200) {
              onRequestCancel();
              Confrim(true);
              // Swal.fire({
              //   icon: "success",
              //   title: "ผิดพลาด",
              //   text: "รหัสผ่านถูกต้อง",
              // });
            } else {
              Swal.fire({
                icon: "error",
                title: "ผิดพลาด",
                text: "รหัสผ่านไม่ถูกต้อง!",
              });
              Dot_1("dot ml-1 mr-1");
              Dot_2("dot ml-1 mr-1");
              Dot_3("dot ml-1 mr-1");
              Dot_4("dot ml-1 mr-1");
              Dot_5("dot ml-1 mr-1");
              Dot_6("dot ml-1 mr-1");
              SetPin("");
            }
          },
          (error) => {
            console.log(error);
          }
        );
      }
    }
  };
  return (
    <>
      <div hidden={!confrim} style={{ height: "90vh" }}>
        <div
          style={{
            height: "90vh",
            backgroundColor: "rgb(156, 0, 0)",
            overflow: "scroll",
          }}
          hidden={pending}
        >
          <div className="d-flex justify-content-center">
            <img
              src="/image/logo.svg"
              className="d-flex justify-content-center"
              width={"300px"}
              height={"150px"}
            />
          </div>
          <div
            className="d-flex justify-content-center mt-2"
            style={{ color: "#fff", fontFamily: "SukhumvitSet-SemiBold" }}
          >
            <label>ยกเลิกการออม</label>
          </div>
          <div className="d-flex justify-content-center mt-2">
            <div
              className="card"
              style={{
                width: "90vw",
                backgroundImage:
                  "linear-gradient(rgb(255,255,255), rgb(229,229,229))",
              }}
            >
              <div className="card-body">
                <div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="exampleRadios"
                      id="exampleRadios1"
                      defaultValue="option1"
                      defaultChecked
                      onClick={onClickBath}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="exampleRadios1"
                    >
                      เงิน
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="exampleRadios"
                      id="exampleRadios2"
                      defaultValue="option2"
                      onClick={onClickGold}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="exampleRadios2"
                    >
                      ทอง
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="d-flex justify-content-center mt-2">
            <div
              className="card"
              style={{
                width: "90vw",
                backgroundImage:
                  "linear-gradient(rgb(255,255,255), rgb(229,229,229))",
              }}
            >
              <div className="card-body ">
                <div
                  className="d-flex justify-content-center col"
                  style={{
                    fontFamily: "SukhumvitSet-SemiBold",
                    color: "rgb(37,37,37)",
                  }}
                >
                  <label>
                    ทองสะสม{" "}
                    {parseFloat(price / total_savings / 15.16).toFixed(2)} กรัม
                  </label>
                </div>
                <div className="d-flex justify-content-center col">
                  <input
                    className="form-control"
                    disabled={true}
                    style={{
                      textAlign: "center",
                      backgroundColor: "rgb(236,216,146)",
                      color: "#fff",
                    }}
                    value={total_savings}
                  />
                </div>
                <div className="d-flex justify-content-center col">
                  <label>ค่าธรรมเนียม{fee}</label>
                </div>
              </div>
            </div>
          </div>
          <div hidden={btn_cancel}>
            <div className="d-flex justify-content-center mt-2">
              <button
                type="button"
                className="btn  d-flex justify-content-center"
                style={{
                  width: "30vw",
                  backgroundImage: "linear-gradient(rgb(101,0,0), rgb(53,0,0))",
                  color: "#fff",
                  fontFamily: "SukhumvitSet-SemiBold",
                }}
                onClick={onConfirmPass}
              >
                ยืนยันการถอน
              </button>
            </div>
          </div>
        </div>
        <div
          style={{
            height: "90vh",
            backgroundColor: "rgb(156, 0, 0)",
            overflow: "scroll",
          }}
          hidden={!pending}
        >
          <div className="d-flex justify-content-center">
            <img
              src="/image/logo.svg"
              className="d-flex justify-content-center"
              width={"300px"}
              height={"150px"}
            />
          </div>
          <div
            className="d-flex justify-content-center mt-2"
            style={{ color: "#fff", fontFamily: "SukhumvitSet-SemiBold" }}
          >
            <label className="ml-2 mr-2">{unready_text}</label>
          </div>
        </div>
      </div>
      {/* ////////////////////////////// */}
      <div style={{ height: "90vh" }} hidden={confrim}>
        <div className="container-fluid" style={{ height: "5vh" }}>
          <div className="container-fluid">
            <div className="row d-flex justify-content-end align-items-center mr-2 pt-3">
              <label onClick={closeConfrim}>X</label>
            </div>
          </div>
        </div>
        <div
          style={{
            height: "20vh",
            alignItems: "center",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <form>
            <div className="form-group d-flex justify-content-center">
              <label>ยืนยันรหัสผ่านของคุณ</label>
            </div>
            <span className={dot_1} />
            <span className={dot_2} />
            <span className={dot_3} />
            <span className={dot_4} />
            <span className={dot_5} />
            <span className={dot_6} />
          </form>
        </div>
        <div className="container-fluid" style={{ height: "60vh" }}>
          <div className="container-fluid">
            <div className="row d-flex justify-content-around">
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("1")}
                >
                  1
                </button>
              </div>
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("2")}
                >
                  2
                </button>
              </div>
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("3")}
                >
                  3
                </button>
              </div>
            </div>
          </div>

          <div className="container-fluid mt-3">
            <div className="row d-flex justify-content-around">
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("4")}
                >
                  4
                </button>
              </div>
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("5")}
                >
                  5
                </button>
              </div>
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("6")}
                >
                  6
                </button>
              </div>
            </div>
          </div>
          <div className="container-fluid mt-3">
            <div className="row d-flex justify-content-around">
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("7")}
                >
                  7
                </button>
              </div>
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("8")}
                >
                  8
                </button>
              </div>
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("9")}
                >
                  9
                </button>
              </div>
            </div>
          </div>
          <div className="container-fluid mt-3">
            <div className="row d-flex justify-content-around">
              <div className="col d-flex justify-content-center"></div>
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("0")}
                >
                  0
                </button>
              </div>
              <div className="col d-flex justify-content-center">
                <button
                  className="btn-no-focus"
                  style={ButtonStyle}
                  onClick={() => OnPinPress("x")}
                >
                  x
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
