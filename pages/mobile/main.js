import React, { useState, useEffect } from "react";
import axios, { post } from "axios";
import { useRouter } from "next/router";
import Modal from "react-bootstrap/Modal";
import moment from "moment";
import Swal from "sweetalert2";

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
export default function LabelBottomNavigation() {
  const router = useRouter();
  const [createroom, Createroom] = React.useState(false);
  const [joinroom, Joinroom] = React.useState(false);
  const [member, Member] = useState(String);
  const [password, Password] = useState(String);
  const [weight, Weight] = useState("น้ำหนักทอง");
  const [btncreroom, Btncreroom] = useState("สร้างห้อง+");
  const [btncreroom_dis, Btncreroom_dis] = useState(false);
  const [btnjoinroom_dis, Btnjoinroom_dis] = useState(false);
  const [btncreroom_hide, Btncreroom_hide] = useState(true);
  const [btnjoinroom_hide, Btnjoinroom_hide] = useState(true);
  const [owner_status, Owner_status] = useState("");

  const [hidealert, Hidealert] = useState(true);
  const [goldprice, setGoldprice] = useState(Array);

  const [minmember, Minmember] = useState(String);
  const [installment, Installment] = useState(String);
  const [day, Day] = useState(String);
  const [fee, Fee] = useState(String);
  useEffect(() => {
    LoadData();
    GoldPrice();
    Status_User();
  }, []);

  const LoadData = async () => {
    await axios({
      method: "GET",
      url: "/api/saving_room_setting/save_setting",
    }).then((response) => {
      Minmember(response.data.data[0].min_member);
      Installment(response.data.data[0].installment);
      Day(response.data.data[0].day);
      Fee(response.data.data[0].fee);
    });
  };
  const GoldPrice = async () => {
    await axios({
      method: "get",
      url: "https://thai-gold-api.herokuapp.com/latest",
    }).then((response) => {
      // console.log(response.data.response.price.gold);
      setGoldprice(response.data.response.price.gold);
    });
  };
  const Status_User = async () => {
    var data = {
      user_id: "id1",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_rooms/check_owner_and_joining",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response.status);
        if (response.status == 201) {
          Btnjoinroom_hide(false);
          Btncreroom_hide(false);
        }
        if (response.status == 202) {
          Btnjoinroom_hide(true);
          Btncreroom_hide(false);
        }
        if (response.status == 203) {
          Btnjoinroom_hide(false);
          Btncreroom_hide(true);
          Owner_status("รอดำเนินการ");
        }
        if (response.status == 204) {
          Btnjoinroom_hide(true);
          Btncreroom_hide(true);
          Owner_status("รอดำเนินการ");
        }
        if (response.status == 205) {
          Btnjoinroom_hide(false);
          Btncreroom_hide(true);
          Owner_status("พร้อมใช้งาน");
        }
        if (response.status == 206) {
          Btnjoinroom_hide(true);
          Btncreroom_hide(true);
          Owner_status("พร้อมใช้งาน");
        }
      },
      (error) => {
        console.log(error);
      }
    );
  };

  const OnSendrequest = async (e) => {
    if (member < minmember) {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "จำนวนสมาชิกต้องไม่ต่ำกว่า " + minmember + " คน",
      });
    }
    if (weight == "น้ำหนักทอง") {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "กรุณาระบุน้ำหนักทอง ",
      });
    }

    var room_id = "saveing_" + moment(new Date()).format("YYYYMMDD_HHmmss");
    var token = makeid(13);
    console.log(parseInt(installment) * parseInt(day));

    var d = new Date();
    d.setDate(d.getDate() + parseInt(installment) * parseInt(day));
    // console.log(d);
    var price = 0;
    if (weight == "ครึ่งสลึง") {
      price = goldprice.buy / 8;
    }
    if (weight == "1 สลึง") {
      price = goldprice.buy / 4;
    }
    if (weight == "2 สลึง") {
      price = goldprice.buy / 2;
    }
    if (weight == "1 บาท") {
      price = goldprice.buy;
    }
    var data = {
      room_id: room_id,
      token: token,
      owner: "Uf08604dbfae9ffdc8123032a8cb2d26c",
      weight: weight,
      price: goldprice.buy,
      status: "pending",
      start_date: new Date(),
      end_date: d,
      day: day,
      fee: fee,
      installment: installment,
      member: member,
    };

    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_rooms/request_room",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response);
        // ReloadRegisterData();
        // setShow(false);
        // this.setState({ modal_show: false });
        if (response.status == 200) {
          Createroom(false);
          Status_User();
          return Swal.fire({
            icon: "success",
            title: "เรียบร้อย",
            text: "ส่งคำขอสร้างห้องสำเร็จ",
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  };
  const OnJoinRoom = async (e) => {
    // console.log(password);
    var data = {
      token: password,
    };
    var data_room = await axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_rooms/check_readyroom",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        if (response.status == 200) {
          return response.data.data;
        }
        if (response.status == 201) {
          return [];
        }
      },
      (error) => {
        console.log(error);
      }
    );
    var user_id = "test_id";
    var owner = data_room[0].owner;

    if (owner == user_id) {
      return Swal.fire({
        icon: "error",
        title: "ผิดพลาด",
        text: "คุณไม่สามารถเข้าร่วมห้องที่คุณสร้างขึ้นได้",
      });
    }
    // console.log(data_room[0].member);
    var data = {
      room_id: data_room[0].room_id,
      user_id: user_id,
      total_savings: "0",
      join_date: "",
      total_installment: "0",
      finish_date: "",
      max_member: data_room[0].member,
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/joinroom/join_room",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        if (response.status == 200) {
          localStorage.setItem("Room_id", data_room[0].room_id);
          return router.push("/mobile/saving_room");
        }
        if (response.status == 201) {
          return Swal.fire({
            icon: "error",
            title: "ผิดพลาด",
            text: "สมาชิกในห้องเต็มแล้ว",
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  };
  const onChangeMember = async (e) => {
    const { value } = e.target;
    const message = value.slice(0, 5);
    if (isNaN(message)) {
    } else {
      await Member(message);
    }
  };
  const onChangeWeight = async (e) => {
    const { value } = e.target;
    // console.log(value);
    Weight(value);
  };
  const owner_room = async () => {
    // await axios({
    //   method: "get",
    //   url: "https://thai-gold-api.herokuapp.com/latest",
    // }).then((response) => {
    //   // console.log(response.data.response.price.gold);
    //   setGoldprice(response.data.response.price.gold);
    // });
    return router.push("/mobile/owner_room");
  };
  const saving_room = async () => {
    // await axios({
    //   method: "get",
    //   url: "https://thai-gold-api.herokuapp.com/latest",
    // }).then((response) => {
    //   // console.log(response.data.response.price.gold);
    //   setGoldprice(response.data.response.price.gold);
    // });
    return router.push("/mobile/saving_room");
  };
  const onCheckMin = async () => {
    if ((await parseInt(member)) < (await parseInt(minmember))) {
      await Hidealert(false);
    } else {
      await Hidealert(true);
    }
  };
  return (
    <>
      <div
        style={{
          height: "90vh",
          backgroundColor: "rgb(156, 0, 0)",
          overflow: "scroll",
        }}
      >
        <div className="d-flex justify-content-center">
          <img
            src="/image/logo.svg"
            className="d-flex justify-content-center"
            width={"300px"}
            height={"150px"}
            //
          />
        </div>
        <div className="d-flex justify-content-center">
          <div>
            <button
              type="button"
              className="btn  btn-sm"
              style={{
                backgroundColor: "#fff",
                width: "30vw",
                fontFamily: "SukhumvitSet-SemiBold",
              }}
              hidden={btncreroom_hide}
              // disabled={btncreroom_dis}
              onClick={() => Createroom(true)}
            >
              สร้างห้อง++
            </button>

            <button
              type="button"
              className="btn  btn-sm ml-2"
              style={{
                backgroundColor: "#fff",
                width: "30vw",
                fontFamily: "SukhumvitSet-SemiBold",
              }}
              hidden={btnjoinroom_hide}
              // disabled={btnjoinroom_dis}
              onClick={() => Joinroom(true)}
            >
              เข้าร่วมห้อง
            </button>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-2">
          <div className="card" style={{ width: "90vw" }}>
            <div className="card-body">
              <div className="container">
                <div className="row">
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(153,0,0)",
                      fontSize: "3vw",
                    }}
                  >
                    ราคาทองแม่อุ้ย
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    ซื้อเข้า
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(0,190,22)",
                      fontSize: "3vw",
                    }}
                  >
                    ขายออก
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    เที่ยบราคาเมื่อวาน
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(231,180,0)",
                      fontSize: "3vw",
                    }}
                  >
                    96.5%
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    {goldprice.buy}
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(0,190,22)",
                      fontSize: "3vw",
                    }}
                  >
                    {goldprice.sell}
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    +100
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-2">
          <div className="card" style={{ width: "90vw" }}>
            <div className="card-body">
              <div className="container">
                <div className="row">
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(153,0,0)",
                      fontSize: "3vw",
                    }}
                  >
                    ราคาทองสมาคม
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    ซื้อเข้า
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(0,190,22)",
                      fontSize: "3vw",
                    }}
                  >
                    ขายออก
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    เที่ยบราคาเมื่อวาน
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(231,180,0)",
                      fontSize: "3vw",
                    }}
                  >
                    96.5%
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    {goldprice.buy}
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(0,190,22)",
                      fontSize: "3vw",
                    }}
                  >
                    {goldprice.sell}
                  </div>
                  <div
                    className="col-3"
                    style={{
                      fontFamily: "SukhumvitSet-SemiBold",
                      color: "rgb(255,6,6)",
                      fontSize: "3vw",
                    }}
                  >
                    +100
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-2">
          <div
            className="card"
            style={{
              width: "90vw",
              backgroundImage: "linear-gradient(rgb(101,0,0), rgb(53,0,0))",
              color: "#fff",
              fontSize: "10pt",
            }}
            onClick={owner_room}
            hidden={!btncreroom_hide}
          >
            <div className="card-body">
              <div className="form-row">
                <div className="col">
                  <p className="card-text">ตำแหน่ง : เจ้าของห้อง</p>
                </div>
                <div className="col">
                  <p className="card-text">สถานะ : {owner_status} </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-2  mb-5">
          <div
            className="card"
            style={{
              width: "90vw",
              backgroundImage: "linear-gradient(rgb(101,0,0), rgb(53,0,0))",
              color: "#fff",
              fontSize: "10pt",
            }}
            onClick={saving_room}
            hidden={!btnjoinroom_hide}
          >
            <div className="card-body">
              <div className="form-row">
                <div className="col">
                  <p className="card-text">ตำแหน่ง : สมาชิก</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        size="xl"
        show={createroom}
        onHide={() => Createroom(false)}
        dialogClassName="modal-90w mt-30h"
        aria-labelledby="example-custom-modal-styling-title"
        centered
      >
        <Modal.Body>
          <form
            style={{
              maxHeight: "60vh",
              overflow: "scroll",
            }}
          >
            <div className="modal-body ">
              <p
                className="d-flex justify-content-center"
                style={{
                  color: "rgb(153, 0, 0)",
                  fontFamily: "SukhumvitSet-SemiBold",
                }}
              >
                สร้างห้องออมทอง
              </p>

              <input
                type="text"
                className="form-control InputRegister d-flex justify-content-center"
                style={{ backgroundColor: "rgb(153, 0, 0)", color: "#fff" }}
                placeholder={"จำนวนสมาชิกต้องไม่ต่ำกว่า  " + minmember}
                value={member}
                onChange={(e) => onChangeMember(e)}
                onMouseLeave={() => onCheckMin()}
              />
              <p hidden={hidealert}>*จำนวนสมาชิกต้องไม่ต่ำกว่า {minmember}</p>
              <select
                id="inputState"
                className="form-control mt-2"
                style={{ backgroundColor: "rgb(153, 0, 0)", color: "#fff" }}
                onChange={(e) => onChangeWeight(e)}
              >
                <option selected disabled>
                  น้ำหนักทอง
                </option>
                <option>ครึ่งสลึง</option>
                <option>1 สลึง</option>
                <option>2 สลึง</option>
                <option>1 บาท</option>
              </select>
              <div className="d-flex justify-content-center">
                <button
                  type="button"
                  className="btn btn-primary mt-2 "
                  onClick={OnSendrequest}
                  style={{
                    textAlign: "center",
                    backgroundColor: "rgb(236,216,146)",
                    color: "#fff",
                  }}
                >
                  ยืนยัน
                </button>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <Modal
        size="xl"
        show={joinroom}
        onHide={() => Joinroom(false)}
        dialogClassName="modal-90w mt-30h"
        aria-labelledby="example-custom-modal-styling-title"
        centered
      >
        <Modal.Body>
          <form
            style={{
              maxHeight: "60vh",
              overflow: "scroll",
            }}
          >
            <div className="modal-body">
              <p
                className="d-flex justify-content-center"
                style={{
                  color: "rgb(153, 0, 0)",
                  fontFamily: "SukhumvitSet-SemiBold",
                }}
              >
                เข้าร่วมห้อง
              </p>
              <input
                type="password"
                className="form-control InputRegister d-flex justify-content-center"
                style={{ backgroundColor: "rgb(153, 0, 0)", color: "#fff" }}
                placeholder={"รหัสเข้าร่วมห้อง"}
                value={password}
                onChange={(e) => Password(e.target.value)}
              />
              <div className="d-flex justify-content-center">
                <button
                  type="button"
                  className="btn btn-primary mt-2 justify-content-center"
                  onClick={OnJoinRoom}
                  style={{
                    textAlign: "center",
                    backgroundColor: "rgb(236,216,146)",
                    color: "#fff",
                  }}
                >
                  ยืนยัน
                </button>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
}
