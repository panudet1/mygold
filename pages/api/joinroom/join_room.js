import dbConnect from "../../../utils/dbConnect";
import JoinRooms from "../../../models/JoinRooms";
import hisJoinRooms from "../../../models/history/hisJoinRooms";
dbConnect();

export default async (req, res) => {
  const { method } = req;

  switch (method) {
    case "POST":
      try {
        // console.log(req.body.data);
        const joinrooms = await JoinRooms.find({
          room_id: req.body.data.room_id,
        });
        if (joinrooms.length < req.body.data.max_member) {
          var item = {
            room_id: req.body.data.room_id,
            user_id: req.body.data.user_id,
            status: "saving",
            total_savings: "0",
            join_date: new Date(),
            total_installment: "0",
            finish_date: "",
          };
          const joinrooms = await JoinRooms.create(item);
          const hisjoinrooms = await hisJoinRooms.create(item);
          res.status(200).json({ success: true, data: joinrooms });
        } else {
          res.status(201).json({ success: true, data: "max" });
        }
        res.status(200).json({ success: true, data: req.body.data });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "GET":
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
};
