import dbConnect from "../../../utils/dbConnect";
import Bill from "../../../models/Bill";
import JoinRooms from "../../../models/JoinRooms";
import SavingRooms from "../../../models/SavingRooms";
import hisBill from "../../../models/history/hisBill";
dbConnect();

export default async (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      try {
        const filter = { _id: req.body.data.id };
        const update = {
          status: "approved",
        };
        let document = await Bill.findOneAndUpdate(filter, update);
        let item = {
          user_id: document.user_id,
          time: document.time,
          money: document.money,
          status: "approved",
          bill_path: document.bill_path,
          room_id: document.room_id,
          upload_date: new Date(),
          modify_by: req.body.data.modify_by,
        };
        await hisBill.create(item);
        const beforeData = await JoinRooms.find({
          user_id: req.body.data.user_id,
        });
        const total_saving =
          parseInt(beforeData[0].total_savings) + parseInt(req.body.data.money);
        const room = await SavingRooms.find({
          room_id: beforeData[0].room_id,
        });

        if (room[0].price <= total_saving) {
          const filter = { _id: beforeData[0]._id };
          const update = {
            finish_date: new Date(),
            total_savings: total_saving,
            status: "finished",
            total_installment: parseInt(beforeData[0].total_installment) + 1,
          };
          let doc = await JoinRooms.findOneAndUpdate(filter, update);
          res.status(200).json({ success: true, data: doc });
        } else {
          const filter = { _id: beforeData[0]._id };
          const update = {
            total_savings: total_saving,
            total_installment: parseInt(beforeData[0].total_installment) + 1,
          };
          let doc = await JoinRooms.findOneAndUpdate(filter, update);
          res.status(200).json({ success: true, data: doc });
        }
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
};
