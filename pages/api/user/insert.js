import dbConnect from "../../../utils/dbConnect";
import User from "../../../models/User";
import hisUser from "../../../models/history/hisUser";
dbConnect();

export default async (req, res) => {
  const { method } = req;

  switch (method) {
    case "POST":
      var item = {
        user_id: req.body.user_line_id,
        password: "",
        role: "user",
        fname: req.body.data.fname,
        lname: req.body.data.lname,
        phonenumber: req.body.data.phonenumber,
        idcard: req.body.data.idcard,
        brithday: req.body.data.brithday,
        expiry: req.body.data.expiry,
        backidcard: req.body.data.backidcard,
        job: req.body.data.job,
        job_location: req.body.data.job_location,
        address: req.body.data.address,
        province: req.body.data.province,
        district: req.body.data.district,
        canton: req.body.data.canton,
        postal_code: req.body.data.postal_code,
        user_line_id: req.body.data.user_line_id,
        fileIdCard: req.body.data.fileIdCard,
        fileUserWithIdCard: req.body.data.fileUserWithIdCard,
        status: "nopass",
        dateCreate: new Date(),
        createby: req.body.user_id,
      };
      const hisuser = await hisUser.create(item);
      const user = await User.create(item);
      res.status(200).json({ success: true, data: user });

      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
};
