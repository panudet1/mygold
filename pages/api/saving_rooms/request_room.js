import dbConnect from "../../../utils/dbConnect";
import SavingRooms from "../../../models/SavingRooms";
import hisSavingRooms from "../../../models/history/hisSavingRooms";
dbConnect();

export default async (req, res) => {
  const { method } = req;

  switch (method) {
    case "POST":
      try {
        var item = {
          room_id: req.body.data.room_id,
          token: req.body.data.token,
          owner: req.body.data.owner,
          weight: req.body.data.weight,
          price: req.body.data.price,
          status: req.body.data.status,
          start_date: req.body.data.start_date,
          end_date: req.body.data.end_date,
          day: req.body.data.day,
          fee: req.body.data.fee,
          installment: req.body.data.installment,
          member: req.body.data.member,
          approve_by: "",
          created_date: new Date(),
        };
        const savingrooms = await SavingRooms.create(item);
        const hissavingRooms = await hisSavingRooms.create(item);
        res.status(200).json({ success: true, data: savingrooms });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "GET":
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
};
