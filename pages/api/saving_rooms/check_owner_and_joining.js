import dbConnect from "../../../utils/dbConnect";
import SavingRooms from "../../../models/SavingRooms";
import JoinRooms from "../../../models/JoinRooms";
dbConnect();

export default async (req, res) => {
  const { method } = req;

  switch (method) {
    case "POST":
      try {
        const savingrooms = await SavingRooms.find({
          status: { $in: ["ready", "pending"] },
          // status: "ready",
          owner: req.body.data.user_id,
        });
        const joinrooms = await JoinRooms.find({
          status: "saving",
          user_id: req.body.data.user_id,
        });

        if (savingrooms.length == 0) {
          if (joinrooms.length == 0) {
            res.status(201).json({ success: true });
          } else {
            res.status(202).json({ success: true });
          }
        } else {
          if (savingrooms[0].status == "pending") {
            if (joinrooms.length == 0) {
              res.status(203).json({ success: true });
            } else {
              res.status(204).json({ success: true });
            }
          }
          if (savingrooms[0].status == "ready") {
            if (joinrooms.length == 0) {
              res.status(205).json({ success: true });
            } else {
              res.status(206).json({ success: true });
            }
          }
        }
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "GET":
      try {
        const savingrooms = await SavingRooms.find({
          status: "pending",
          owner: "",
        });

        res.status(200).json({ success: true, data: savingrooms });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
};
