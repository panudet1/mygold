// import withSession from "../../lib/session";

// export default withSession(async (req, res) => {
//   req.session.destroy();
//   res.json({ isLoggedIn: false });
// });

import Cookies from "cookies";
export default async (req, res) => {
  const cookies = new Cookies(req, res);
  cookies.set("user");
  res.json({ isLoggedIn: false });
};
