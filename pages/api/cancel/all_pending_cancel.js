import dbConnect from "../../../utils/dbConnect";
import Cancel from "../../../models/Cancel";

dbConnect();

export default async (req, res) => {
  const { method } = req;

  switch (method) {
    case "GET":
      try {
        const cancel = await Cancel.find({ status: "pending" });
        res.status(200).json({ success: true, data: cancel });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    // case "GET":
    //   break;
    default:
      res.status(400).json({ success: false });
      break;
  }
};
