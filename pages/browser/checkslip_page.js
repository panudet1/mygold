import React, { useState, useEffect } from "react";
import Cookies from "cookies";
import axios, { post } from "axios";
import Typography from "@material-ui/core/Typography";
import { MDBDataTableV5 } from "mdbreact";
import moment from "moment";
import Dropdown from "react-bootstrap/Dropdown";
import Modal from "react-bootstrap/Modal";

const CheckslipPage = ({ user }) => {
  const [seleteddata, Seleteddata] = useState(Array);
  const [datatable, setDatatable] = useState(Array);
  const [slip_img, Slip_img] = useState(String);
  const [show, setShow] = useState(false);
  const [_id, _Id] = useState(String);
  useEffect(async () => {
    LoadDataSlip();
  }, []);

  const LoadDataSlip = async () => {
    var dataslip = await axios({
      method: "get",
      url: "/api/bill/check_all_bill_pending",
    }).then((response) => {
      return response.data.data;
    });
    var data = [];
    dataslip.forEach((element) => {
      var row = {
        name: "ชื่อ-นามสกุล",
        line_id: element.user_id,
        money: element.money,
        date: moment(element.upload_date).format("YYYY-MM-DD HH:mm:ss"),
        action: (
          <Dropdown>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
            ></Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item onClick={() => OnOpenDetail(element)}>
                ดูข้อมูล
              </Dropdown.Item>
              {/* <Dropdown.Item onClick={() => OnDelete(element._id)}>
                ลบ
              </Dropdown.Item> */}
            </Dropdown.Menu>
          </Dropdown>
        ),
      };
      data.push(row);
    });
    var columns = [
      {
        label: "ชื่อ-นามสกุล",
        field: "name",
        width: 150,
      },
      {
        label: "Line-Id",
        field: "line_id",
        width: 270,
      },
      {
        label: "จำนวนเงิน",
        field: "money",
        sort: "asc",
        width: 200,
      },
      {
        label: "วันที่ทำรายการ",
        field: "date",
        sort: "asc",
        width: 200,
      },
      {
        label: "Action",
        field: "action",
        sort: "disable",
        width: 100,
      },
    ];
    var rows = data;
    var detail = { columns, rows };
    setDatatable(detail);
  };

  const OnOpenDetail = async (e) => {
    const word1 = await e.bill_path.replaceAll("\\", "/");
    var res1 = word1.slice(6);
    console.log(res1);
    Seleteddata(e);
    _Id(e._id);
    Slip_img(res1);
    setShow(true);
  };
  const OnApprove = async (e) => {
    var data = {
      id: e._id,
      user_id: e.user_id,
      money: e.money,
      modify_by: "admin",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/bill/bill_approve",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
    LoadDataSlip();
    setShow(false);
  };
  const OnNotApprove = async (e) => {
    var data = {
      id: e,
      modify_by: "admin",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/bill/bill_decline",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
    LoadDataSlip();
    setShow(false);
  };
  return (
    <>
      <Typography paragraph>
        <div className="card ">
          <div className="card-header">ตรวจสอบสลิป</div>
          <div className="card-body">
            <MDBDataTableV5
              hover
              entriesOptions={[5, 20, 25]}
              entries={5}
              pagesAmount={4}
              data={datatable}
              pagingTop
              searchTop
              searchBottom={false}
            />
          </div>
        </div>
        <Modal
          size="xl"
          show={show}
          dialogClassName="modal-90w mt-30h"
          aria-labelledby="example-custom-modal-styling-title"
          centered
        >
          <Modal.Header closeButton onClick={() => setShow(false)}>
            <Modal.Title id="example-custom-modal-styling-title">
              ข้อมูลการโอน
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form
              style={{
                maxHeight: "60vh",
                overflow: "scroll",
              }}
            >
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  ชื่อ-นามสกุล:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={"ชื่อ-นามสกุล"}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  จำนวนเงิน:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.money}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  เวลาในสลิปโอนเงิน:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.time}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  รูปสลิป:
                </label>
                <div className="col-sm-3"></div>
              </div>
              <div className="form-group row">
                <div className="col-sm-5">
                  <img
                    src={slip_img}
                    className="d-flex justify-content-center"
                    width={"300px"}
                    height={"400px"}
                  />
                  {/* lastted */}
                </div>
              </div>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <button
              onClick={() => OnNotApprove(_id)}
              type="button"
              className="btn btn-danger"
            >
              ไม่อนุมัติ
            </button>
            <button
              onClick={() => OnApprove(seleteddata)}
              type="button"
              className="btn btn-success"
            >
              อนุมัติ
            </button>
          </Modal.Footer>
        </Modal>
      </Typography>
    </>
  );
};
//   <span onClick={OnLogout}>Go To login</span> */}
export const getServerSideProps = async ({ req, res }) => {
  const cookies = new Cookies(req, res);
  const user = await cookies.get("user");
  console.log(user);
  if (!user) {
    res.statusCode = 404;
    res.end();
    return { props: {} };
  }
  return {
    props: { user },
  };
};

export default CheckslipPage;
