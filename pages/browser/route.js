import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { useRouter } from "next/router";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import Dashboard from "@material-ui/icons/Dashboard";
import Class from "@material-ui/icons/Class";
import People from "@material-ui/icons/People";
import LocalAtm from "@material-ui/icons/LocalAtm";
import AssignmentTurnedIn from "@material-ui/icons/AssignmentTurnedIn";
import AttachMoney from "@material-ui/icons/AttachMoney";
import Timeline from "@material-ui/icons/Timeline";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Button from "@material-ui/core/Button";
import axios, { post } from "axios";
import { ThemeProvider } from "@material-ui/styles";
import theme from "../../components/theme.js";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Dashboard_page from "./dashboard_page";
import Manageroom_page from "./manageroom_page";
import Manageremember_page from "./manageremember_page";
import Checkslip_page from "./checkslip_page";
import Cancel_page from "./cancel_page";
import History_page from "./history_page";
import Withdraw_page from "./withdraw_page";
const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));
const Header = () => {
  const classes = useStyles();
  // const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  // const [loop, setLoop] = React.useState(0);
  const [register, Register] = useState({
    empty: true,
    alert: false,
    count: "0",
  });
  const [bill, Bill] = useState({
    empty: true,
    alert: false,
    count: "0",
  });
  const [withdraw, Withdraw] = useState({
    empty: true,
    alert: false,
    count: "0",
  });
  const [cancel, Cancel] = useState({
    empty: true,
    alert: false,
    count: "0",
  });
  const [savingroom, Savingroom] = useState({
    empty: true,
    alert: false,
    count: "0",
  });
  useEffect(() => {
    CheckStaus();
    // console.log("check");
  }, []);
  setInterval(() => {
    CheckStaus();
    // console.log("check every 30 ");
  }, 1000 * 30);
  const CheckStaus = async () => {
    axios.get("/api/check_status").then(function (response) {
      if (response.data.data.Register_ > 0) {
        Register({
          empty: false,
          alert: true,
          count: response.data.data.Register_,
        });
      } else {
        Register({
          empty: true,
          alert: false,
          count: "0",
        });
      }
      if (response.data.data.Bill_ > 0) {
        Bill({
          empty: false,
          alert: true,
          count: response.data.data.Bill_,
        });
      } else {
        Bill({
          empty: true,
          alert: false,
          count: "0",
        });
      }
      if (response.data.data.Withdraw_ > 0) {
        Withdraw({
          empty: false,
          alert: true,
          count: response.data.data.Withdraw_,
        });
      } else {
        Withdraw({
          empty: true,
          alert: false,
          count: "0",
        });
      }
      if (response.data.data.Cancel_ > 0) {
        Cancel({
          empty: false,
          alert: true,
          count: response.data.data.Cancel_,
        });
      } else {
        Cancel({
          empty: true,
          alert: false,
          count: "0",
        });
      }
      if (response.data.data.SavingRooms_ > 0) {
        Savingroom({
          empty: false,
          alert: true,
          count: response.data.data.SavingRooms_,
        });
      } else {
        Savingroom({
          empty: true,
          alert: false,
          count: "0",
        });
      }
    });
  };
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const router = useRouter();
  const OnLogout = async (e) => {
    e.preventDefault();
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      url: "/api/logout",
    }).then(
      (response) => {
        return router.push("/browser/login");
      },
      (error) => {
        console.log(error);
      }
    );
  };

  return (
    <>
      <Router>
        <ThemeProvider theme={theme}>
          <div className={classes.root}>
            <CssBaseline />
            <AppBar
              position="fixed"
              className={clsx(classes.appBar, {
                [classes.appBarShift]: open,
              })}
            >
              <Toolbar className="d-flex bd-highlight mb-3">
                <IconButton
                  className="p-2 bd-highlight"
                  color="inherit"
                  aria-label="open drawer"
                  onClick={handleDrawerOpen}
                  edge="start"
                  className={clsx(classes.menuButton, {
                    [classes.hide]: open,
                  })}
                >
                  <MenuIcon />
                </IconButton>
                <Typography className="p-2 bd-highlight" variant="h6" noWrap>
                  ร้านทองอารมณ์ดี (แอดมิน)
                </Typography>
                <div className="ml-auto p-2 bd-highlight">
                  <Button
                    onClick={OnLogout}
                    variant="contained"
                    color="default"
                    className={classes.button}
                    endIcon={<ExitToAppIcon />}
                  >
                    ออกจากระบบ
                  </Button>
                </div>
              </Toolbar>
            </AppBar>
            <Drawer
              variant="permanent"
              className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
              })}
              classes={{
                paper: clsx({
                  [classes.drawerOpen]: open,
                  [classes.drawerClose]: !open,
                }),
              }}
            >
              <div className={classes.toolbar}>
                <IconButton onClick={handleDrawerClose}>
                  {theme.direction === "rtl" ? (
                    <ChevronRightIcon />
                  ) : (
                    <ChevronLeftIcon />
                  )}
                </IconButton>
              </div>
              <Divider />
              <List>
                <Link to="/">
                  <ListItem>
                    <ListItemIcon>
                      <Dashboard />
                    </ListItemIcon>
                    <ListItemText primary={"แดชบอร์ด"} />
                  </ListItem>
                </Link>
                <Link to="/manageroom">
                  <ListItem>
                    <ListItemIcon>
                      <Class hidden={savingroom.alert} />
                      <Class
                        style={{ color: "#E74C3C" }}
                        hidden={savingroom.empty}
                      />
                      <div
                        hidden={savingroom.empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {savingroom.count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"จัดการห้องออม"} />
                  </ListItem>
                </Link>
                <Link to="/manageremember">
                  <ListItem>
                    <ListItemIcon>
                      <People hidden={register.alert} />
                      <People
                        style={{ color: "#E74C3C" }}
                        hidden={register.empty}
                      />
                      <div
                        hidden={register.empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {register.count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"จัดการสมาชิก"} />
                  </ListItem>
                </Link>
                <Link to="/cancel">
                  <ListItem>
                    <ListItemIcon>
                      <LocalAtm hidden={cancel.alert} />
                      <LocalAtm
                        style={{ color: "#E74C3C" }}
                        hidden={cancel.empty}
                      />
                      <div
                        hidden={cancel.empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {cancel.count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"ยกเลิกออม"} />
                  </ListItem>
                </Link>
                <Link to="/withdraw">
                  <ListItem>
                    <ListItemIcon>
                      <AttachMoney hidden={withdraw.alert} />
                      <AttachMoney
                        style={{ color: "#E74C3C" }}
                        hidden={withdraw.empty}
                      />
                      <div
                        hidden={withdraw.empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {withdraw.count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"รายการถอน"} />
                  </ListItem>
                </Link>
                <Link to="/checkslip">
                  <ListItem>
                    <ListItemIcon>
                      <AssignmentTurnedIn hidden={bill.alert} />
                      <AssignmentTurnedIn
                        style={{ color: "#E74C3C" }}
                        hidden={bill.empty}
                      />
                      <div
                        hidden={bill.empty}
                        style={{
                          width: "20px",
                          height: "20px",
                          backgroundColor: "#E74C3C ",
                          borderRadius: "10px",
                          textAlign: "center",
                          color: "#fff",
                        }}
                      >
                        {bill.count}
                      </div>
                    </ListItemIcon>
                    <ListItemText primary={"ตรวจสอบสลิป"} />
                  </ListItem>
                </Link>
                <Link to="/history">
                  <ListItem>
                    <ListItemIcon>
                      <Timeline />
                    </ListItemIcon>
                    <ListItemText primary={"ข้อมูลย้อนหลัง"} />
                  </ListItem>
                </Link>
              </List>
            </Drawer>
            <main className={classes.content}>
              <div className={classes.toolbar} />
              <Typography>
                <Switch>
                  <Route path="/withdraw">
                    <Withdraw_page />
                  </Route>
                  <Route path="/history">
                    <History_page />
                  </Route>
                  <Route path="/cancel">
                    <Cancel_page />
                  </Route>
                  <Route path="/checkslip">
                    <Checkslip_page />
                  </Route>
                  <Route path="/manageremember">
                    <Manageremember_page />
                  </Route>
                  <Route path="/manageroom">
                    <Manageroom_page />
                  </Route>
                  <Route path="/">
                    <Dashboard_page />
                  </Route>
                </Switch>
              </Typography>
            </main>
          </div>
        </ThemeProvider>
      </Router>
    </>
  );
};
export default Header;
