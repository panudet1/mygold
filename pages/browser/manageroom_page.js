import React, { useState, useEffect } from "react";
import axios, { post } from "axios";
import Cookies from "cookies";
import Typography from "@material-ui/core/Typography";

import { MDBDataTableV5 } from "mdbreact";
import Dropdown from "react-bootstrap/Dropdown";
import Modal from "react-bootstrap/Modal";
import moment from "moment";
import Swal from "sweetalert2";

const ManageroomPage = ({ user }) => {
  const [fixbutton, Fixbutton] = useState(false);
  const [member, Member] = useState(String);
  const [installment, Installment] = useState(String);
  const [day, Day] = useState(String);
  const [fee, Fee] = useState(String);
  const [datatable, setDatatable] = useState(Array);
  const [datatableroom, setDatatableroom] = useState(Array);
  const [seleteddata, Seleteddata] = useState(Array);
  const [show, setShow] = useState(false);
  const [showroomdetail, setShowroomdetail] = useState(false);
  useEffect(() => {
    LoadData();
    LoadRequestRoom();
    LoadAllRoom();
  }, []);

  const LoadData = async () => {
    await axios({
      method: "GET",
      url: "/api/saving_room_setting/save_setting",
    }).then((response) => {
      Member(response.data.data[0].min_member);
      Installment(response.data.data[0].installment);
      Day(response.data.data[0].day);
      Fee(response.data.data[0].fee);
    });
  };
  const LoadAllRoom = async () => {
    var Allrequest = await axios({
      method: "get",
      url: "/api/saving_rooms/all_room",
    }).then((response) => {
      return response.data.data;
    });
    var data = [];
    Allrequest.forEach((element) => {
      var row = {
        room_id: element.room_id,
        line_id: element.owner,
        start_date: moment(element.start_date).format("YYYY-MM-DD"),
        end_date: moment(element.end_date).format("YYYY-MM-DD"),
        created_date: moment(element.created_date).format(
          "YYYY-MM-DD HH:mm:ss"
        ),
        action: (
          <Dropdown>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
            ></Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item onClick={() => OnOpenReadyRoomDetail(element)}>
                ดูข้อมูล
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        ),
      };
      data.push(row);
    });
    var columns = [
      {
        label: "room_id",
        field: "room_id",
        width: 150,
      },
      {
        label: "Line-Id",
        field: "line_id",
        width: 150,
      },
      {
        label: "เริ่ม",
        field: "start_date",
        sort: "asc",
        width: 150,
      },
      {
        label: "สิ้นสุด",
        field: "end_date",
        width: 150,
      },
      {
        label: "สร้างเมื่อ",
        field: "created_date",
        width: 150,
      },
      {
        label: "Action",
        field: "action",
        sort: "disable",
        width: 100,
      },
    ];
    var rows = data;
    var detail = { columns, rows };
    setDatatableroom(detail);
  };
  const LoadRequestRoom = async () => {
    var Allrequest = await axios({
      method: "get",
      url: "/api/saving_rooms/all_request",
    }).then((response) => {
      return response.data.data;
    });
    var data = [];
    Allrequest.forEach((element) => {
      // console.log(element);
      var row = {
        room_id: element.room_id,
        line_id: element.owner,
        start_date: moment(element.start_date).format("YYYY-MM-DD"),
        end_date: moment(element.end_date).format("YYYY-MM-DD"),
        created_date: moment(element.created_date).format(
          "YYYY-MM-DD HH:mm:ss"
        ),
        action: (
          <Dropdown>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
            ></Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item onClick={() => OnOpenDetail(element)}>
                ดูข้อมูล
              </Dropdown.Item>
              {/* <Dropdown.Item onClick={() => OnDelete(element._id)}>
                ลบ
              </Dropdown.Item> */}
            </Dropdown.Menu>
          </Dropdown>
        ),
      };
      data.push(row);
    });
    var columns = [
      {
        label: "room_id",
        field: "room_id",
        width: 150,
      },
      {
        label: "Line-Id",
        field: "line_id",
        width: 150,
      },
      {
        label: "เริ่ม",
        field: "start_date",
        sort: "asc",
        width: 150,
      },
      {
        label: "สิ้นสุด",
        field: "end_date",
        width: 150,
      },
      {
        label: "สร้างเมื่อ",
        field: "created_date",
        width: 150,
      },
      {
        label: "Action",
        field: "action",
        sort: "disable",
        width: 100,
      },
    ];
    var rows = data;
    var detail = { columns, rows };
    setDatatable(detail);
  };
  const OnOpenDetail = async (e) => {
    Seleteddata(e);
    setShow(true);
  };
  const OnOpenReadyRoomDetail = async (e) => {
    setShowroomdetail(true);
    Seleteddata(e);
    // setShow(true);
  };
  const OnApprove = async (e) => {
    // console.log(e);
    var data = {
      id: e._id,
      user_id: "admin",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_rooms/update_approve",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        LoadRequestRoom();
        setShow(false);
        return Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "อนุมัติสร้างห้องสำเร็จ",
        });
      },
      (error) => {
        console.log(error);
      }
    );
    LoadRequestRoom();
    setShow(false);
  };
  const OnNotApprove = async (e) => {
    // console.log(e);
    var data = {
      id: e._id,
      user_id: "admin",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_rooms/update_decline",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        LoadRequestRoom();
        setShow(false);
        return Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "ปฏิเสธสร้างห้องสำเร็จ",
        });
      },
      (error) => {
        console.log(error);
      }
    );
    LoadRequestRoom();
    setShow(false);
    // const defult =
    //   'public\register\upload_4e73bb3f255315a20f8ea8a2425304b6.jpg';
    // setShow_detail(false);OnApprove
  };
  const OnDelete = async (e) => {
    var data = {
      id: e,
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_rooms/delete_request",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        // console.log(response);
        LoadRequestRoom();
        setShow(false);
        // this.setState({ modal_show: false });
        return Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "ลบข้อมูลการขอสร้างห้องสำเร็จ",
        });
      },
      (error) => {
        console.log(error);
      }
    );
  };
  const SaveSetting = () => {
    var data = {
      min_member: member,
      installment: installment,
      day: day,
      fee: fee,
      user_id: "admin",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/saving_room_setting/save_setting",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response);
        Fixbutton(false);
      },
      (error) => {
        console.log(error);
      }
    );
  };

  const onChangeMember = (e) => {
    const { value } = e.target;
    const message = value.slice(0, 13);
    if (isNaN(message)) {
    } else {
      Member(message);
    }
  };
  const onChangeInstallment = (e) => {
    const { value } = e.target;
    const message = value.slice(0, 13);
    if (isNaN(message)) {
    } else {
      Installment(message);
    }
  };
  const onChangeDay = (e) => {
    const { value } = e.target;
    const message = value.slice(0, 13);
    if (isNaN(message)) {
    } else {
      Day(message);
    }
  };
  const onChangeFee = (e) => {
    const { value } = e.target;
    const message = value.slice(0, 13);
    if (isNaN(message)) {
    } else {
      Fee(message);
    }
  };
  return (
    <>
      <Typography paragraph>
        <div className="card mt-5">
          <div className="card-header">คำขอสร้างห้อง</div>
          <div className="card-body">
            <MDBDataTableV5
              hover
              entriesOptions={[5, 20, 25]}
              entries={5}
              pagesAmount={4}
              data={datatable}
              pagingTop
              searchTop
              searchBottom={false}
            />
          </div>
        </div>
        <div className="card mt-5">
          <div className="card-header">ตั้งค่าข้อมูลห้องออม</div>
          <div className="card-body">
            <div className="form-group">
              <label>สมาชิกขั้นต่ำ (คน)</label>
              <input
                disabled={!fixbutton}
                type="text"
                className="form-control"
                value={member}
                onChange={(e) => onChangeMember(e)}
                placeholder="จำนวนสมาชิกขั้นต่ำในการขอสร้างห้องออม"
              />
            </div>
            <div className="form-group">
              <label>จำนวนงวด</label>
              <input
                disabled={!fixbutton}
                type="text"
                className="form-control"
                value={installment}
                onChange={(e) => onChangeInstallment(e)}
                placeholder="จำนวนงวดในการออม"
              />
            </div>
            <div className="form-group">
              <label>ระยะเวลาต่องวด (วัน)</label>
              <input
                disabled={!fixbutton}
                type="text"
                className="form-control"
                value={day}
                onChange={(e) => onChangeDay(e)}
                placeholder="ระยะเวลาต่องวด (จำนวนวัน)"
              />
            </div>
            <div className="form-group">
              <label>ค่าธรรมเนี่ยม (บาท)</label>
              <input
                disabled={!fixbutton}
                type="text"
                className="form-control"
                value={fee}
                onChange={(e) => onChangeFee(e)}
                placeholder="ค่าธรรมเนี่ยม (บาท)"
              />
            </div>
            <button
              className="btn btn-primary"
              hidden={fixbutton}
              onClick={() => Fixbutton(true)}
            >
              แก้ไข
            </button>
            <button
              type="button"
              hidden={!fixbutton}
              className="btn btn-success"
              onClick={() => SaveSetting()}
            >
              บันทึก
            </button>
            <button
              type="button"
              className="btn btn-danger"
              hidden={!fixbutton}
              onClick={() => Fixbutton(false)}
            >
              ยกเลิก
            </button>
          </div>
        </div>
        <div className="card mt-5">
          <div className="card-header">ภาพรวม</div>
          <div className="card-body">
            <MDBDataTableV5
              hover
              entriesOptions={[5, 20, 25]}
              entries={5}
              pagesAmount={4}
              data={datatableroom}
              pagingTop
              searchTop
              searchBottom={false}
            />
          </div>
        </div>

        <Modal
          size="xl"
          show={show}
          dialogClassName="modal-90w mt-30h"
          aria-labelledby="example-custom-modal-styling-title"
          centered
        >
          <Modal.Header closeButton onClick={() => setShow(false)}>
            <Modal.Title id="example-custom-modal-styling-title">
              ข้อมูลคำขอสร้างห้อง
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form
              style={{
                maxHeight: "60vh",
                overflow: "scroll",
              }}
            >
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  แม่ออม:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.owner}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  token_id:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.token}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  น้ำหนักทอง:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.weight}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  ราคาบาทละ:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.price}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  เริ่มออม:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.start_date}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  สิ้นสุด:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.end_date}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  จำนวนงวด:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.installment}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  ระยะเวลาต่องวด(วัน):
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.day}
                  />
                </div>
              </div>
              {/* <div className="form-group row">

                  </div> */}
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  จำนวนสมาชิก:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.member}
                  />
                </div>
              </div>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <button
              onClick={() => OnNotApprove(seleteddata)}
              type="button"
              className="btn btn-danger"
            >
              ไม่อนุมัติ
            </button>
            <button
              onClick={() => OnApprove(seleteddata)}
              type="button"
              className="btn btn-success"
            >
              อนุมัติ
            </button>
          </Modal.Footer>
        </Modal>
        <Modal
          size="xl"
          show={showroomdetail}
          dialogClassName="modal-90w mt-30h"
          aria-labelledby="example-custom-modal-styling-title"
          centered
        >
          <Modal.Header closeButton onClick={() => setShowroomdetail(false)}>
            <Modal.Title id="example-custom-modal-styling-title">
              ข้อมูลห้อง
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form
              style={{
                maxHeight: "60vh",
                overflow: "scroll",
              }}
            >
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  แม่ออม:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.owner}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  token_id:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.token}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  น้ำหนักทอง:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.weight}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  ราคาบาทละ:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.price}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  เริ่มออม:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.start_date}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  สิ้นสุด:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.end_date}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  จำนวนงวด:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.installment}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  ระยะเวลาต่องวด(วัน):
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.day}
                  />
                </div>
              </div>
              {/* <div className="form-group row">

                  </div> */}
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  จำนวนสมาชิก:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.member}
                  />
                </div>
              </div>
            </form>
          </Modal.Body>
        </Modal>
      </Typography>
    </>
  );
};
export const getServerSideProps = async ({ req, res }) => {
  const cookies = new Cookies(req, res);
  const user = await cookies.get("user");
  console.log(user);
  if (!user) {
    res.statusCode = 404;
    res.end();
    return { props: {} };
  }
  return {
    props: { user },
  };
};

export default ManageroomPage;
