import React, { useState, useEffect } from "react";
import Cookies from "cookies";
import axios, { post } from "axios";
import Typography from "@material-ui/core/Typography";

const DashboardPage = ({ user }) => {
  const [name, setName] = useState(String);
  const [time, setTime] = useState(String);
  const [gold, setGold] = useState(Array);
  const [gold_bar, setGold_bar] = useState(Array);
  useEffect(() => {
    axios({
      method: "get",
      url: "https://thai-gold-api.herokuapp.com/latest",
    }).then((response) => {
      // console.log(response.data.response.date);
      setName(response.data.response.date);
      setTime(response.data.response.update_time);
      setGold(response.data.response.price.gold);
      setGold_bar(response.data.response.price.gold_bar);
    });
  }, []);
  // const Test = axiosTest();
  setInterval(
    () =>
      axios({
        method: "get",
        url: "https://thai-gold-api.herokuapp.com/latest",
      }).then((response) => {
        console.log(response.data.response.date);
        setName(response.data.response.date);
        setTime(response.data.response.update_time);
        setGold(response.data.response.price.gold);
        setGold_bar(response.data.response.price.gold_bar);
      }),
    1000 * 60 * 15
  );

  return (
    <>
      <Typography paragraph>วันที่ {name}</Typography>
      <Typography paragraph> {time}</Typography>
      <Typography paragraph>
        ทองคำแทง ซื้อ {gold.buy} ขาย {gold.sell}
      </Typography>
      <Typography paragraph>
        ทองรูปพรรณ ซื้อ {gold_bar.buy} ขาย {gold_bar.sell}
      </Typography>
    </>
  );
};
//   <span onClick={OnLogout}>Go To login</span> */}
export const getServerSideProps = async ({ req, res }) => {
  const cookies = new Cookies(req, res);
  const user = await cookies.get("user");
  if (!user) {
    res.statusCode = 404;
    res.end();
    return { props: {} };
  }
  return {
    props: { user },
  };
};

export default DashboardPage;
