import React from "react";
import Cookies from "cookies";
import Typography from "@material-ui/core/Typography";

const HistoryPage = ({ user }) => {
  return (
    <>
      <Typography paragraph>HistoryPage</Typography>
    </>
  );
};
//   <span onClick={OnLogout}>Go To login</span> */}
export const getServerSideProps = async ({ req, res }) => {
  const cookies = new Cookies(req, res);
  const user = await cookies.get("user");
  console.log(user);
  if (!user) {
    res.statusCode = 404;
    res.end();
    return { props: {} };
  }
  return {
    props: { user },
  };
};

export default HistoryPage;
