import React, { useEffect } from "react";
import { useRouter } from "next/router";
import Cookies from "cookies";

const useUser = () => ({ user: null, loading: false });
const IndexPage = ({ loged }) => {
  const router = useRouter();
  const { user, loading } = useUser();
  if (!loged) {
    useEffect(() => {
      if (!(user || loading)) {
        router.push("/browser/login");
      }
    }, [user, loading]);
    return <p></p>;
  } else {
    useEffect(() => {
      if (!(user || loading)) {
        router.push("/browser/route");
      }
    }, [user, loading]);
    return <p></p>;
  }
};

// IndexPage.getInitialProps = async ({ req, res }) => {
//   const cookies = new Cookies(req, res);
//   const loged = await cookies.get("user");
//   return { loged };
// };

export const getServerSideProps = async ({ req, res }) => {
  const cookies = new Cookies(req, res);
  const loged = await cookies.get("user");

  // console.log(loged);

  if (loged === undefined) {
    return {
      props: {},
    };
  } else {
    return {
      props: { loged },
    };
  }
};

export default IndexPage;
