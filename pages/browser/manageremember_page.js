import React, { useState, useEffect } from "react";
import Cookies from "cookies";
import axios, { post } from "axios";
import Typography from "@material-ui/core/Typography";
import Modal from "react-bootstrap/Modal";
import Image from "next/image";
import { MDBDataTableV5 } from "mdbreact";
import Dropdown from "react-bootstrap/Dropdown";
import Swal from "sweetalert2";
import moment from "moment";

const ManagerememberPage = ({ user }) => {
  const [show, setShow] = useState(false);
  const [fileIdCard, FileIdCard] = useState(String);
  const [fileUserWithIdCard, FileUserWithIdCard] = useState(String);
  const [datatable, setDatatable] = useState(Array);
  const [datatableuser, setDatatableuser] = useState(Array);
  const [seleteddata, Seleteddata] = useState(Array);
  const [_id, _Id] = useState(String);
  useEffect(async () => {
    ReloadRegisterData();
    ReloadUserData();
  }, []);
  const OnOpenDetail = async (e) => {
    const word1 = await e.fileIdCard.replaceAll("\\", "/");
    var res1 = word1.slice(6);
    const word2 = await e.fileUserWithIdCard.replaceAll("\\", "/");
    var res2 = word2.slice(6);

    // console.log(e.fileIdCard, e.fileIdCard, res1, res2);
    Seleteddata(e);
    _Id(e._id);
    FileIdCard(e.fileIdCard);
    FileUserWithIdCard(e.fileUserWithIdCard);
    setShow(true);
  };
  const OnDelete = async (e) => {
    var data = {
      id: e,
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/register/delete",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        console.log(response);
        ReloadRegisterData();
        setShow(false);
        // this.setState({ modal_show: false });
        return Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "ลบข้อมูลการสมัครสำเร็จ",
        });
      },
      (error) => {
        console.log(error);
      }
    );
  };

  const OnApprove = async (e) => {
    console.log(e);
    var data = {
      id: e._id,
    };
    var main = {
      user_id: "admin",
      data: e,
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/register/update_approve",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {},
      (error) => {
        console.log(error);
      }
    );
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/user/insert",
      data: JSON.stringify(main),
    }).then(
      (response) => {
        return Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "เพิ่มข้อมูลผู้ใช้สำเร็จ",
        });
      },
      (error) => {
        console.log(error);
      }
    );
    ReloadRegisterData();
    setShow(false);
  };

  const OnCloseDetail = async (e) => {
    setShow(false);
  };

  const ReloadUserData = async () => {
    var register = await axios({
      method: "get",
      url: "/api/user/all_user",
    }).then((response) => {
      return response.data.data;
    });
    var data = [];
    register.forEach((element) => {
      console.log(element.fname);
      var row = {
        name: element.fname + " " + element.lname,
        line_id: element.user_line_id,
        date: moment(element.dateRegister).format("YYYY-MM-DD HH:mm:ss"),
        action: (
          <Dropdown>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
            ></Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item onClick={() => OnOpenDetail(element)}>
                ดูข้อมูล
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        ),
      };
      data.push(row);
    });
    var columns = [
      {
        label: "ชื่อ-นามสกุล",
        field: "name",
        width: 150,
      },
      {
        label: "Line-Id",
        field: "line_id",
        width: 270,
      },
      {
        label: "วันที่สมัคร",
        field: "date",
        sort: "asc",
        width: 200,
      },
      {
        label: "Action",
        field: "action",
        sort: "disable",
        width: 100,
      },
    ];
    var rows = data;
    var detail = { columns, rows };
    setDatatableuser(detail);
  };
  const ReloadRegisterData = async () => {
    var register = await axios({
      method: "get",
      url: "/api/register",
    }).then((response) => {
      return response.data.data;
    });
    var data = [];
    register.forEach((element) => {
      console.log(element.fname);
      var row = {
        name: element.fname + " " + element.lname,
        line_id: element.user_line_id,
        date: moment(element.dateRegister).format("YYYY-MM-DD HH:mm:ss"),
        action: (
          <Dropdown>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
            ></Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item onClick={() => OnOpenDetail(element)}>
                ดูข้อมูล
              </Dropdown.Item>
              <Dropdown.Item onClick={() => OnDelete(element._id)}>
                ลบ
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        ),
      };
      data.push(row);
    });
    var columns = [
      {
        label: "ชื่อ-นามสกุล",
        field: "name",
        width: 150,
      },
      {
        label: "Line-Id",
        field: "line_id",
        width: 270,
      },
      {
        label: "วันที่สมัคร",
        field: "date",
        sort: "asc",
        width: 200,
      },
      {
        label: "Action",
        field: "action",
        sort: "disable",
        width: 100,
      },
    ];
    var rows = data;
    var detail = { columns, rows };
    setDatatable(detail);
  };
  return (
    <>
      <Typography paragraph>
        <div className="card ">
          <div className="card-header">ข้อมูลสมาชิก</div>
          <div className="card-body">
            <MDBDataTableV5
              hover
              entriesOptions={[5, 20, 25]}
              entries={5}
              pagesAmount={4}
              data={datatableuser}
              pagingTop
              searchTop
              searchBottom={false}
            />
          </div>
        </div>

        <div className="card mt-5">
          <div className="card-header">คำขอสมัครสมาชิก</div>
          <div className="card-body">
            <MDBDataTableV5
              hover
              entriesOptions={[5, 20, 25]}
              entries={5}
              pagesAmount={4}
              data={datatable}
              pagingTop
              searchTop
              searchBottom={false}
            />
          </div>
        </div>
        <Modal
          size="xl"
          show={show}
          dialogClassName="modal-90w mt-30h"
          aria-labelledby="example-custom-modal-styling-title"
          centered
        >
          <Modal.Header closeButton onClick={() => OnCloseDetail()}>
            <Modal.Title id="example-custom-modal-styling-title">
              ข้อมูลผู้สมัคร
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form
              style={{
                maxHeight: "60vh",
                overflow: "scroll",
              }}
            >
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  ชื่อ-นามสกุล:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.fname + "  " + seleteddata.lname}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  เบอร์โทรศัพท์มือถือ:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.phonenumber}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  หมายเลขบัตรประชาชน:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.idcard}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  หมายเลขหลังบัตรประชาชน:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.backidcard}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  วัน/เดือน/ปีเกิด:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.brithday}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  วันหมดอายุบัตรประชาชน:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.expiry}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  อาชีพ:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.job}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  สถานที่ทำงาน:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.job_location}
                  />
                </div>
              </div>

              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  บ้านเลขที่:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.address}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  จังหวัด:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.province}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  อำเภอ:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.district}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  ตำบล:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.canton}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  รหัสไปรษณีย์:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.postal_code}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  รูปบัตรประชาชน:
                </label>
                <div className="col-sm-3"></div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  รูปถ่ายคู่บัตรประชาชน:
                </label>
                <div className="col-sm-3"></div>
              </div>
              <div className="form-group row">
                <div className="col-sm-5">
                  <img
                    src={fileIdCard}
                    className="d-flex justify-content-center"
                    width={"500px"}
                    height={"270px"}
                  />
                </div>
                <div className="col-sm-5">
                  <img
                    src={fileUserWithIdCard}
                    className="d-flex justify-content-center"
                    width={"500px"}
                    height={"270px"}
                  />
                </div>
              </div>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <button
              onClick={() => OnDelete(_id)}
              type="button"
              className="btn btn-danger"
            >
              ลบ
            </button>
            <button
              onClick={() => OnApprove(seleteddata)}
              type="button"
              className="btn btn-success"
            >
              อนุมัติ
            </button>
          </Modal.Footer>
        </Modal>
      </Typography>
    </>
  );
};

export const getServerSideProps = async ({ req, res }) => {
  const cookies = new Cookies(req, res);
  const user = await cookies.get("user");
  if (!user) {
    res.statusCode = 404;
    res.end();
    return { props: {} };
  }
  return {
    props: { user },
  };
};

export default ManagerememberPage;
