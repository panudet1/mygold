import React, { useState, useEffect } from "react";
import Cookies from "cookies";
import Header from "../../components/Header";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import { MDBDataTableV5 } from "mdbreact";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { ThemeProvider } from "@material-ui/styles";
import theme from "../../components/theme.js";
import { EditSharp } from "@material-ui/icons";
import axios, { post } from "axios";
import moment from "moment";
import Dropdown from "react-bootstrap/Dropdown";
import Modal from "react-bootstrap/Modal";
import Swal from "sweetalert2";

const PromotionPage = ({ user }) => {
  const [seleteddata, Seleteddata] = useState(Array);
  const [datatable, setDatatable] = useState(Array);
  const [show, setShow] = useState(false);
  const [confrim_pass, Confrim_pass] = useState(true);

  useEffect(async () => {
    LoadDataWithdraw();
  }, []);

  const OnOpenDetail = async (e) => {
    Seleteddata(e);
    setShow(true);
  };
  const LoadDataWithdraw = async () => {
    var dataWithdraw = await axios({
      method: "get",
      url: "/api/withdraw/all_pending_withdraw",
    }).then((response) => {
      return response.data.data;
    });
    console.log(dataWithdraw);
    var data = [];
    dataWithdraw.forEach((element) => {
      var row = {
        user_id: element.user_id,
        type: element.type,
        total: element.total,
        date: moment(element.withdraw_date).format("YYYY-MM-DD HH:mm:ss"),
        action: (
          <Dropdown>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
            ></Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item onClick={() => OnOpenDetail(element)}>
                ดูข้อมูล
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        ),
      };
      data.push(row);
    });
    var columns = [
      {
        label: "user_id",
        field: "user_id",
        width: 150,
      },
      {
        label: "ประเภท",
        field: "type",
        width: 270,
      },
      {
        label: "จำนวน",
        field: "total",
        sort: "asc",
        width: 200,
      },
      {
        label: "วันที่",
        field: "date",
        sort: "asc",
        width: 200,
      },
      {
        label: "Action",
        field: "action",
        sort: "disable",
        width: 100,
      },
    ];
    var rows = data;
    var detail = { columns, rows };
    setDatatable(detail);
  };
  const OnCloseDetail = async (e) => {
    setShow(false);
  };
  const OnDontApprove = async (e) => {
    // console.log(e);
    var data = {
      id: e._id,
      user_id: e.user_id,
      room_id: e.room_id,
      modify_by: "admin",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/withdraw/decline_withdraw",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        // console.log(response);
        return Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "ปฏิเสธการถอน สำเร็จ",
        });
      },
      (error) => {
        console.log(error);
      }
    );
    await LoadDataWithdraw();
    setShow(false);
  };
  const OnApprove = async (e) => {
    console.log(e);
    var data = {
      id: e._id,
      user_id: e.user_id,
      room_id: e.room_id,
      modify_by: "admin",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/withdraw/approve_withdraw",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        // console.log(response);
        return Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "อนุมัติการถอน สำเร็จ",
        });
      },
      (error) => {
        console.log(error);
      }
    );
    await LoadDataWithdraw();
    setShow(false);
  };
  return (
    <>
      <Typography paragraph>
        <div className="card ">
          <div className="card-header">ตรวจสอบรายการถอน เงิน-ทอง</div>
          <div className="card-body">
            <MDBDataTableV5
              hover
              entriesOptions={[5, 20, 25]}
              entries={5}
              pagesAmount={4}
              data={datatable}
              pagingTop
              searchTop
              searchBottom={false}
            />
          </div>
        </div>
        <Modal
          size="xl"
          show={show}
          dialogClassName="modal-90w 50h mt-30h"
          aria-labelledby="example-custom-modal-styling-title"
          centered
        >
          <Modal.Header closeButton onClick={() => OnCloseDetail()}>
            <Modal.Title id="example-custom-modal-styling-title">
              ข้อมูลการถอน
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form
              style={{
                minHeight: "30vh",
                maxHeight: "60vh",
                overflow: "scroll",
              }}
            >
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  user_id:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.user_id}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  ประเภท:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.type}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  จำนวน:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.total}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  วันที่:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.withdraw_date}
                  />
                </div>
              </div>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <button
              onClick={() => OnDontApprove(seleteddata)}
              type="button"
              className="btn btn-danger"
            >
              ไม่อนุมัติ
            </button>
            <button
              onClick={() => OnApprove(seleteddata)}
              type="button"
              className="btn btn-success"
            >
              อนุมัติ
            </button>
          </Modal.Footer>
        </Modal>
      </Typography>
    </>
  );
};
//   <span onClick={OnLogout}>Go To login</span> */}
export const getServerSideProps = async ({ req, res }) => {
  const cookies = new Cookies(req, res);
  const user = await cookies.get("user");
  console.log(user);
  if (!user) {
    res.statusCode = 404;
    res.end();
    return { props: {} };
  }
  return {
    props: { user },
  };
};

export default PromotionPage;
