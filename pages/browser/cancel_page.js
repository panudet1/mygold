import React, { useState, useEffect } from "react";
import Cookies from "cookies";
import { MDBDataTableV5 } from "mdbreact";
import Typography from "@material-ui/core/Typography";
import axios, { post } from "axios";
import moment from "moment";
import Dropdown from "react-bootstrap/Dropdown";
import Modal from "react-bootstrap/Modal";
import Swal from "sweetalert2";

const PromotionPage = ({ user }) => {
  const [seleteddata, Seleteddata] = useState(Array);
  const [datatable, setDatatable] = useState(Array);
  const [show, setShow] = useState(false);
  useEffect(async () => {
    LoadDataCancel();
  }, []);

  const OnOpenDetail = async (e) => {
    // const word1 = await e.fileIdCard.replaceAll("\\", "/");
    // var res1 = word1.slice(6);
    // const word2 = await e.fileUserWithIdCard.replaceAll("\\", "/");
    // var res2 = word2.slice(6);
    Seleteddata(e);
    // _Id(e._id);
    // FileIdCard(res1);
    // FileUserWithIdCard(res2);
    setShow(true);
  };
  const LoadDataCancel = async () => {
    var dataCencel = await axios({
      method: "get",
      url: "/api/cancel/all_pending_cancel",
    }).then((response) => {
      return response.data.data;
    });
    console.log(dataCencel);
    var data = [];
    dataCencel.forEach((element) => {
      var row = {
        user_id: element.user_id,
        type: element.type,
        total: element.total,
        date: moment(element.cancel_date).format("YYYY-MM-DD HH:mm:ss"),
        action: (
          <Dropdown>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
            ></Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item onClick={() => OnOpenDetail(element)}>
                ดูข้อมูล
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        ),
      };
      data.push(row);
    });
    var columns = [
      {
        label: "user_id",
        field: "user_id",
        width: 150,
      },
      {
        label: "ประเภท",
        field: "type",
        width: 270,
      },
      {
        label: "จำนวน",
        field: "total",
        sort: "asc",
        width: 200,
      },
      {
        label: "วันที่",
        field: "date",
        sort: "asc",
        width: 200,
      },
      {
        label: "Action",
        field: "action",
        sort: "disable",
        width: 100,
      },
    ];
    var rows = data;
    var detail = { columns, rows };
    setDatatable(detail);
  };
  const OnCloseDetail = async (e) => {
    setShow(false);
  };
  const OnDontApprove = async (e) => {
    // console.log(e);
    var data = {
      id: e._id,
      user_id: e.user_id,
      room_id: e.room_id,
      modify_by: "admin",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/cancel/decline_cancel",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        // console.log(response);
        return Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "ปฏิเสธการยกเลิก สำเร็จ",
        });
      },
      (error) => {
        console.log(error);
      }
    );
    await LoadDataCancel();
    setShow(false);
  };
  const OnApprove = async (e) => {
    console.log(e);
    var data = {
      id: e._id,
      user_id: e.user_id,
      modify_by: "admin",
    };
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
      url: "/api/cancel/approve_cancel",
      data: JSON.stringify({ data: data }),
    }).then(
      (response) => {
        // console.log(response);
        return Swal.fire({
          icon: "success",
          title: "เรียบร้อย",
          text: "อนุมัติการยกเลิก สำเร็จ",
        });
      },
      (error) => {
        console.log(error);
      }
    );
    await LoadDataCancel();
    setShow(false);
  };
  return (
    <>
      <Typography paragraph>
        <div className="card ">
          <div className="card-header">ตรวจสอบรายการยกเลิกการออม</div>
          <div className="card-body">
            <MDBDataTableV5
              hover
              entriesOptions={[5, 20, 25]}
              entries={5}
              pagesAmount={4}
              data={datatable}
              pagingTop
              searchTop
              searchBottom={false}
            />
          </div>
        </div>
        <Modal
          size="xl"
          show={show}
          dialogClassName="modal-90w 50h mt-30h"
          aria-labelledby="example-custom-modal-styling-title"
          centered
        >
          <Modal.Header closeButton onClick={() => OnCloseDetail()}>
            <Modal.Title id="example-custom-modal-styling-title">
              ข้อมูลการยกเลิก
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form
              style={{
                minHeight: "30vh",
                maxHeight: "60vh",
                overflow: "scroll",
              }}
            >
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  user_id:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.user_id}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  ประเภท:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.type}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  จำนวน:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.total}
                  />
                </div>
                <label
                  htmlFor="staticEmail"
                  className="col-sm-2 col-form-label"
                >
                  วันที่:
                </label>
                <div className="col-sm-3">
                  <input
                    type="text"
                    readOnly
                    className="form-control-plaintext"
                    defaultValue={seleteddata.cancel_date}
                  />
                </div>
              </div>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <button
              onClick={() => OnDontApprove(seleteddata)}
              type="button"
              className="btn btn-danger"
            >
              ไม่อนุมัติ
            </button>
            <button
              onClick={() => OnApprove(seleteddata)}
              type="button"
              className="btn btn-success"
            >
              อนุมัติ
            </button>
          </Modal.Footer>
        </Modal>
      </Typography>
    </>
  );
};
//   <span onClick={OnLogout}>Go To login</span> */}
export const getServerSideProps = async ({ req, res }) => {
  const cookies = new Cookies(req, res);
  const user = await cookies.get("user");
  console.log(user);
  if (!user) {
    res.statusCode = 404;
    res.end();
    return { props: {} };
  }
  return {
    props: { user },
  };
};

export default PromotionPage;
