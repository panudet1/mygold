const mongoose = require("mongoose");
const hisWithdrawSchema = new mongoose.Schema({
  user_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  room_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  type: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  total: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  withdraw_date: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  status: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  modify_by: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
});
module.exports =
  mongoose.model.hisWithdrawSchema ||
  mongoose.model("hisWithdraw", hisWithdrawSchema);
