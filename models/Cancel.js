const mongoose = require("mongoose");
const CancelSchema = new mongoose.Schema({
  user_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  room_id: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  type: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  total: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  cancel_date: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
  status: {
    type: String,
    maxlength: [300, "Title cannot be more than 40 characters"],
  },
});
module.exports =
  mongoose.model.CancelSchema || mongoose.model("Cancel", CancelSchema);
